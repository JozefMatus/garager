package com.beretis.garager.app.Controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.format.Time;


import com.beretis.garager.app.App;
import com.beretis.garager.app.entity.Car;
import com.beretis.garager.app.entity.Customer;
import com.beretis.garager.app.Tables.CustomersTable;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: beretis
 * Date: 7/12/13
 * Time: 7:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class DatabaseMidleFinger
{
    private SQLiteDatabase database;
    private String[] customersCollums = {CustomersTable.CUSTOMERS_COLUMN_ID, CustomersTable.CUSTOMERS_COLUMN_NAME,
            CustomersTable.CUSTOMERS_COLUMN_PHONE, CustomersTable.CUSTOMERS_COLUMN_LAST_VISIT, CustomersTable.CUSTOMERS_COLUMN_CREATE_DATE};

    public DatabaseMidleFinger(Context context)
    {
        database = App.getDb();
    }

    public int createCustomer(String name, String phone)
    {
        int affectedRows;// to do: change this to return afected rows
        Time now = new Time();
        now.setToNow();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CustomersTable.CUSTOMERS_COLUMN_NAME, name);
        contentValues.put(CustomersTable.CUSTOMERS_COLUMN_PHONE, phone);
        contentValues.put(CustomersTable.CUSTOMERS_COLUMN_CREATE_DATE, now.toString());
        contentValues.put(CustomersTable.CUSTOMERS_COLUMN_LAST_VISIT, now.toString());
        long insertedId = database.insert(CustomersTable.CUSTOMERS_TABLE_NAME, null, contentValues);
//        Cursor cursor = database.query(MySqlLiteHelper.CUSTOMERS_TABLE_NAME, customersCollums, MySqlLiteHelper.CUSTOMERS_COLUMN_ID + " = ?", new String[]{String.valueOf(insertedId)}, null, null, null);
//        cursor.close();
        return (int) insertedId;
    }

    public Customer cursorToCustomer(Cursor customerCursor, Cursor cars)
    {
        Customer customer = null;
        if(customerCursor.moveToFirst())
        {

        customer = new Customer(customerCursor.getString(customerCursor.getColumnIndex(CustomersTable.CUSTOMERS_COLUMN_NAME)),
                customerCursor.getString(customerCursor.getColumnIndex(CustomersTable.CUSTOMERS_COLUMN_PHONE)),
                customerCursor.getString(customerCursor.getColumnIndex(CustomersTable.CUSTOMERS_COLUMN_CREATE_DATE)),
                customerCursor.getString(customerCursor.getColumnIndex(CustomersTable.CUSTOMERS_COLUMN_LAST_VISIT)));
        }

        ArrayList<Car> carList = new ArrayList<Car>();
        if(cars.moveToFirst())
        {

        }

        return customer;
    }

    public Cursor getAllCustomersCursor()
    {
        Cursor cursor = database.query(CustomersTable.CUSTOMERS_TABLE_NAME, customersCollums, null, null, null, null, null );
        return cursor;
    }

//    public Customer getCustomer(int id)
//    {
//        Cursor customer = database.query(CustomersTable.CUSTOMERS_TABLE_NAME, customersCollums, CustomersTable.CUSTOMERS_COLUMN_ID + " = ? ", new String[]{String.valueOf(id)}, null, null, null, null);
//        Cursor customersCars = database.query(CustomerHasCarsTable.CUSTOMER_HAS_CARS_TABLE_NAME, new String[]{CustomerHasCarsTable.CUSTOMER_HAS_CARS_COLUMN_ID, CustomerHasCarsTable.
//                CUSTOMER_HAS_CARS_COLUMN_CUSTOMER, CustomerHasCarsTable.CUSTOMER_HAS_CARS_COLUMN_CAR}, CustomerHasCarsTable.CUSTOMER_HAS_CARS_COLUMN_CUSTOMER + " = ?", new String[]{String.valueOf(id)}, null, null, null);
//        return cursorToCustomer(customer, customersCars);
//    }

    public void dumpCustomersTable()
    {
        database.delete(CustomersTable.CUSTOMERS_TABLE_NAME,null, null);
    }

    public void addTestData()
    {
        createCustomer("Filip", "Maslo");
        createCustomer("Jozef", "Matus");
        createCustomer("Miroslav", "Voda");
        createCustomer("Vojtech", "Drbohlav");
    }

}
