package com.beretis.garager.app.Tables;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by beretis on 2.4.14.
 */
public class PartsTable
{

    public static final String PARTS_TABLE_NAME = "parts";
    public static final String PARTS_COLUMN_ID = "_id";
    public static final String PARTS_COLUMN_TITLE = "part_title";
    public static final String PARTS_COLUMN_SUPPLIER = "part_supplier";
    public static final String PARTS_COLUMN_PURCHASE = "purchizePrize";
    public static final String PARTS_COLUMN_CATEGORY = "category";
    public static final String PARTS_COLUMN_SELL = "sellingPrize";
    public static final String PARTS_COLUMN_BARECODE = "barecode";

    static String DATABASE_CREATE = "CREATE TABLE " + PARTS_TABLE_NAME + " (" +
            PARTS_COLUMN_ID + " TEXT primary key NOT NULL," +
            PARTS_COLUMN_TITLE + " TEXT NOT NULL," +
            PARTS_COLUMN_PURCHASE + " TEXT NOT NULL," +
            PARTS_COLUMN_CATEGORY + " TEXT NOT NULL," +
            PARTS_COLUMN_SELL + " TEXT NOT NULL," +
            PARTS_COLUMN_BARECODE + " TEXT," +
            PARTS_COLUMN_SUPPLIER + " TEXT NOT NULL);";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(PartsTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + PARTS_TABLE_NAME);
        onCreate(database);
    }
}
