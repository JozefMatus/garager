package com.beretis.garager.app.Tables;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by jozefmatus on 06/03/15.
 */
public class CarsPhotoTable
{
    public static final String TABLE_NAME = "cars_photos";
    public static final String CARSPHOTO_COLUMN_ID = "_id";
    public static final String CARSPHOTO_COLUMN_PATH = "path";
    public static final String CARSPHOTO_COLUMN_CAR_ID = "car_id";



    static String DATABASE_CREATE = "CREATE TABLE " + TABLE_NAME + " (" +
            CARSPHOTO_COLUMN_ID + " INTEGER primary key autoincrement," +
            CARSPHOTO_COLUMN_PATH + " TEXT NOT NULL," +
            CARSPHOTO_COLUMN_CAR_ID + " INTEGER NOT NULL," +
            "FOREIGN KEY (" + CARSPHOTO_COLUMN_CAR_ID + ") REFERENCES " + CarsTable.CARS_TABLE_NAME + "(" + CarsTable.CARS_COLUMN_ID + "));";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(PartsTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(database);
    }
}
