package com.beretis.garager.app;

import android.app.Application;
import android.database.sqlite.SQLiteDatabase;

import com.beretis.garager.app.Controller.MySqlLiteHelper;

/**
 * Created by beretis on 31.3.14.
 */
public class App extends Application
{
    private static SQLiteDatabase db;

    public static SQLiteDatabase getDb() {
        return db;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        db = new MySqlLiteHelper(getApplicationContext()).getWritableDatabase();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        db.close();
    }
}
