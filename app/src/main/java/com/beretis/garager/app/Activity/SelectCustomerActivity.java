package com.beretis.garager.app.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.beretis.garager.app.Fragments.SelectCustomerFragment;
import com.beretis.garager.app.R;

/**
 * Created by jozefmatus on 15/09/15.
 */
public class SelectCustomerActivity extends AppCompatActivity
{
    public static Intent newIntent(Context context)
    {
        Intent intent = new Intent(context, SelectCustomerActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null)
        {
            return;
        }
        setContentView(R.layout.activity_select_customer);
        setupActionBar();

        getFragmentManager().beginTransaction().add(R.id.containeris, SelectCustomerFragment.newInstance()).commit();
    }


    @Override
    public void onStart()
    {
        super.onStart();
    }


    @Override
    public void onResume()
    {
        super.onResume();
    }


    @Override
    public void onPause()
    {
        super.onPause();
    }


    @Override
    public void onStop()
    {
        super.onStop();
    }


    @Override
    public void onDestroy()
    {
        super.onDestroy();
    }



    private void setupActionBar()
    {
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        ActionBar bar = getSupportActionBar();
        bar.setDisplayUseLogoEnabled(false);
        bar.setDisplayShowTitleEnabled(false);
        bar.setDisplayShowHomeEnabled(true);
        bar.setDisplayHomeAsUpEnabled(false);
        bar.setHomeButtonEnabled(true);
    }
}
