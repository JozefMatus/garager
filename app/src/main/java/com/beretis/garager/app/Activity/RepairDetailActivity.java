package com.beretis.garager.app.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.beretis.garager.app.Fragments.RepairDetailFragment;
import com.beretis.garager.app.R;

public class RepairDetailActivity extends AppCompatActivity
{
    public static final String ARG_REPIAR_ID = "repiar_id";
    public static final String ARG_EDIT = "mEdit";

    private int mRepairId;
    private boolean mEdit;

    public static Intent newIntent(Context context, int repairId, boolean edit)
    {
        Intent intent = new Intent(context, RepairDetailActivity.class);
        intent.putExtra(ARG_REPIAR_ID, repairId);
        intent.putExtra(ARG_EDIT, edit);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setupActionBar();

        if(getIntent().getExtras() != null)
        {
            mEdit = getIntent().getExtras().getBoolean(ARG_EDIT);
            mRepairId = getIntent().getExtras().getInt(ARG_REPIAR_ID);
        }
        setContentView(R.layout.activity_repair_detail);

        getFragmentManager().beginTransaction().add(R.id.containeris, RepairDetailFragment.newInstance(mRepairId, mEdit)).commit();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_repair_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setupActionBar()
    {
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        ActionBar bar = getSupportActionBar();
        bar.setDisplayUseLogoEnabled(false);
        bar.setDisplayShowTitleEnabled(true);
        bar.setDisplayShowHomeEnabled(true);
        bar.setDisplayHomeAsUpEnabled(false);
        bar.setHomeButtonEnabled(true);
    }
}
