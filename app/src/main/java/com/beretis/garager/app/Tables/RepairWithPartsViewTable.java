package com.beretis.garager.app.Tables;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by jozefmatus on 4/8/15.
 */
public class RepairWithPartsViewTable
{
    public static final String TABLE_NAME = "repairWithParts";

    public static final String COLUMN_ID = RepairsTable.REPAIRS_COLUMN_ID;
    public static final String COLUMN_CAR_ID = "car_id";
    public static final String COLUMN_PRICE = "price";
    public static final String COLUMN_SUBJECT = "subject";
    public static final String COLUMN_INFO = "info";
    public static final String COLUMN_CUSTOMER_ID = "customer_id";
    public static final String COLUMN_IS_MAINTINANCE = "is_maitinance";
    public static final String COLUMN_TIME_PERIOD = "time_period";
    public static final String COLUMN_PART_TITLE = "part_title";
    public static final String COLUMN_PART_SUPPLIER = "part_supplier";
    public static final String COLUMN_PART_ID = "part_id";
    public static final String COLUMN_PART_USAGE_DATE = "part_usage_date";
    public static final String COLUMN_PART_WARRANTY_END_DATE = "waranty_end_date";


    public static final String DATABASE_CREATE =
            "CREATE VIEW " + TABLE_NAME
                    + " AS SELECT " + RepairsTable.REPAIRS_TABLE_NAME + ".*"
                    + ", " + RepairHasPartsTable.REPAIR_HAS_PARTS_TABLE_NAME + "." + RepairHasPartsTable.COLUMN_DATE + " AS " + COLUMN_PART_USAGE_DATE
                    + ", " + RepairHasPartsTable.REPAIR_HAS_PARTS_TABLE_NAME + "." + RepairHasPartsTable.COLUMN_WARRANTY_END_DATE + " AS " + COLUMN_PART_WARRANTY_END_DATE
                    + ", " + PartsTable.PARTS_TABLE_NAME + "." + PartsTable.PARTS_COLUMN_TITLE + " AS " + COLUMN_PART_TITLE
                    + ", " + PartsTable.PARTS_TABLE_NAME + "." + PartsTable.PARTS_COLUMN_SUPPLIER + " AS " + COLUMN_PART_SUPPLIER
                    + " FROM " + RepairsTable.REPAIRS_TABLE_NAME
                    + " LEFT JOIN " + RepairHasPartsTable.REPAIR_HAS_PARTS_TABLE_NAME
                    + " ON " + RepairsTable.REPAIRS_TABLE_NAME + "." + RepairsTable.REPAIRS_COLUMN_ID
                    + " = " + RepairHasPartsTable.REPAIR_HAS_PARTS_TABLE_NAME + "." + RepairHasPartsTable.COLUMN_REPAIR_ID
                    + " LEFT JOIN " + PartsTable.PARTS_TABLE_NAME
                    + " ON " + RepairHasPartsTable.REPAIR_HAS_PARTS_TABLE_NAME + "." + RepairHasPartsTable.COLUMN_PART_ID
                    + " = " + PartsTable.PARTS_TABLE_NAME + "." + PartsTable.PARTS_COLUMN_ID + ";";


    public static void onCreate(SQLiteDatabase database)
    {
        String test = DATABASE_CREATE;
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion)
    {
        Log.w(CustomerWithCarsViewTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP VIEW IF EXISTS " + TABLE_NAME);
        onCreate(database);
    }

}
