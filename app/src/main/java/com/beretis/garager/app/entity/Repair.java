package com.beretis.garager.app.entity;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: beretis
 * Date: 7/10/13
 * Time: 4:53 PM
 * To change this template use File | Settings | File Templates.
 */
@Parcel
public class Repair
{
   String date;
   List<Part> usedParts;
   String subject;
   ArrayList<String> work;


    public Repair(String date, List<Part> usedParts, String subject, ArrayList<String> work)
    {
        this.date = date;
        this.usedParts = usedParts;
        this.subject = subject;
        this.work = work;
    }

    public void setDate(String date)
    {
        this.date = date;
    }

    public void setUsedParts(List<Part> usedParts)
    {
        this.usedParts = usedParts;
    }

    public void setSubject(String subject)
    {
        this.subject = subject;
    }

    public void setWork(ArrayList<String> work)
    {
        this.work = work;
    }

    public String getDate()
    {
        return date;
    }

    public List<Part> getUsedParts()
    {
        return usedParts;
    }

    public String getSubject()
    {
        return subject;
    }

    public ArrayList<String> getWork()
    {
        return work;
    }

}
