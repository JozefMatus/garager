package com.beretis.garager.app.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.beretis.garager.app.Activity.CustomerDetailActivity;
import com.beretis.garager.app.CustomersContentProvider;
import com.beretis.garager.app.Adapters.CustomersCursorAdapter;
import com.beretis.garager.app.Controller.DatabaseMidleFinger;
import com.beretis.garager.app.entity.Customer;
import com.beretis.garager.app.R;

import com.beretis.garager.app.Tables.CustomersTable;


/**
 * A fragment representing a list of Items.
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * Activities containing this fragment MUST implement the {@link }
 * interface.
 */
public class CustomersFragment extends Fragment implements AbsListView.OnItemClickListener, LoaderManager.LoaderCallbacks<Cursor>
{
    private Context context;
    private Cursor cursor;
    private DatabaseMidleFinger databaseMidleFinger;

    private OnFragmentInteractionListener mListener;

    /**
     * The fragment's ListView/GridView.
     */
    private ListView mListView;

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private CustomersCursorAdapter mAdapter;


    // TODO: Rename and change types of parameters
    public static CustomersFragment newInstance(String param1, String param2)
    {
        CustomersFragment fragment = new CustomersFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public CustomersFragment()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (getArguments() != null)
        {

        }
        // TODO: Change Adapter to display your content


        setHasOptionsMenu(true);
    }

    public void addCustomer(Customer customer)
    {
        Intent intent = new Intent(getActivity(), CustomerDetailActivity.class);
        intent.putExtra("customerID", -1);
        intent.putExtra("add", true);
        intent.putExtra("edit", false);
        startActivity(intent);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_customersfragment, container, false);

        // Set the adapter
        mListView = (ListView) view.findViewById(R.id.LW_CustomersFragment);

        fillData();
        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);

        return view;
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try
        {
            mListener = (OnFragmentInteractionListener) activity;
            context = activity.getApplicationContext();


        }
        catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        if (null != mListener)
        {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            Object test = mAdapter.getItem(position);
            mListener.onCustomerSelected(mAdapter.getCursor().getInt(mAdapter.getCursor().getColumnIndex(CustomersTable.CUSTOMERS_COLUMN_ID)));
        }
    }

    /**
     * The default content for this Fragment has a TextView that is shown when
     * the list is empty. If you would like to change the text, call this method
     * to supply the text it should use.
     */
    public void setEmptyText(CharSequence emptyText)
    {
        View emptyView = mListView.getEmptyView();

        if (emptyText instanceof TextView)
        {
            ((TextView) emptyView).setText(emptyText);
        }
    }

    private void fillData()
    {
//        cursor = context.getContentResolver().query(MyContentProvider.CONTENT_URI, null, null, null, MyContentProvider.NAME);
//        mAdapter = new CustomersCursorAdapter(context, cursor, true);

        getLoaderManager().initLoader(0, null, this);
        mAdapter = new CustomersCursorAdapter(context, null, 0);
        mListView.setAdapter(mAdapter);

    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle)
    {
        CursorLoader cursorLoader = new CursorLoader(context,
                CustomersContentProvider.CONTENT_URI_CUSTOMERS, null, null, null, CustomersContentProvider.PROVIDER_NAME);
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor)
    {
//TODO awfull hack u have to do better

        mAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader)
    {
        mAdapter.swapCursor(null);
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener
    {
        // TODO: Update argument type and name
        public void onCustomerSelected(int customerId);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.customers_fragment_menu, menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        switch (item.getItemId())
        {
            case R.id.action_add_customer:
                this.addCustomer(new Customer("picka", "mala"));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
