package com.beretis.garager.app.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.SwitchCompat;
import android.text.method.CharacterPickerDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.beretis.garager.app.Activity.SelectCustomerActivity;
import com.beretis.garager.app.Fragments.Helper.Enums;
import com.beretis.garager.app.R;
import com.beretis.garager.app.entity.BaseEntity;
import com.beretis.garager.app.view.ViewState;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.Calendar;


/**
 * Created by jozefmatus on 25/07/15.
 */
public class AddRepairFragment extends Fragment implements DatePickerDialog.OnDateSetListener
{
	private ViewState mViewState = null;
	private View mRootView;
	private BaseEntity mProduct;
	private Activity mContext;

	private EditText mSubjectET, mDateET, mInfoET, mCustomerET, mCarET, mPriceET;
	private TextInputLayout mDateTIL, mCustomerTIL, mCarTIL;
	private TextView mMaintinanceTextView;
	private SwitchCompat mIsMaintinanceSwitchCompat;



	public static AddRepairFragment newInstance(Enums.FragmentCreationType creationType, int mCustomerId, int mCarId)
	{
		AddRepairFragment fragment = new AddRepairFragment();
		Bundle args = new Bundle();
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		mContext = (Activity) activity;
	}


	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		setRetainInstance(true);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		mRootView = inflater.inflate(R.layout.fragment_add_repair, container, false);
		findViews();
		return mRootView;
	}


	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
        renderView();
	}


	@Override
	public void onStart()
	{
		super.onStart();
	}


	@Override
	public void onResume()
	{
		super.onResume();
	}


	@Override
	public void onPause()
	{
		super.onPause();
	}


	@Override
	public void onStop()
	{
		super.onStop();
	}


	@Override
	public void onDestroyView()
	{
		super.onDestroyView();
		mRootView = null;
	}


	@Override
	public void onDestroy()
	{
		super.onDestroy();

		// cancel async tasks
	}


	@Override
	public void onDetach()
	{
		super.onDetach();
	}


	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		// save current instance state
		super.onSaveInstanceState(outState);
		setUserVisibleHint(true);
	}


	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		// action bar menu
		super.onCreateOptionsMenu(menu, inflater);

		// TODO
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// action bar menu behaviour
		return super.onOptionsItemSelected(item);

		// TODO
	}



	private void showContent()
	{
		// show content container
		ViewGroup containerContent = (ViewGroup) mRootView.findViewById(R.id.container_content);
		ViewGroup containerProgress = (ViewGroup) mRootView.findViewById(R.id.container_progress);
		ViewGroup containerOffline = (ViewGroup) mRootView.findViewById(R.id.container_offline);
		ViewGroup containerEmpty = (ViewGroup) mRootView.findViewById(R.id.container_empty);
		containerContent.setVisibility(View.VISIBLE);
		containerProgress.setVisibility(View.GONE);
		containerOffline.setVisibility(View.GONE);
		containerEmpty.setVisibility(View.GONE);
		mViewState = ViewState.CONTENT;
	}


	private void showProgress()
	{
		// show progress container
		ViewGroup containerContent = (ViewGroup) mRootView.findViewById(R.id.container_content);
		ViewGroup containerProgress = (ViewGroup) mRootView.findViewById(R.id.container_progress);
		ViewGroup containerOffline = (ViewGroup) mRootView.findViewById(R.id.container_offline);
		ViewGroup containerEmpty = (ViewGroup) mRootView.findViewById(R.id.container_empty);
		containerContent.setVisibility(View.GONE);
		containerProgress.setVisibility(View.VISIBLE);
		containerOffline.setVisibility(View.GONE);
		containerEmpty.setVisibility(View.GONE);
		mViewState = ViewState.PROGRESS;
	}


	private void showOffline()
	{
		// show offline container
		ViewGroup containerContent = (ViewGroup) mRootView.findViewById(R.id.container_content);
		ViewGroup containerProgress = (ViewGroup) mRootView.findViewById(R.id.container_progress);
		ViewGroup containerOffline = (ViewGroup) mRootView.findViewById(R.id.container_offline);
		ViewGroup containerEmpty = (ViewGroup) mRootView.findViewById(R.id.container_empty);
		containerContent.setVisibility(View.GONE);
		containerProgress.setVisibility(View.GONE);
		containerOffline.setVisibility(View.VISIBLE);
		containerEmpty.setVisibility(View.GONE);
		mViewState = ViewState.OFFLINE;
	}


	private void showEmpty()
	{
		// show empty container
		ViewGroup containerContent = (ViewGroup) mRootView.findViewById(R.id.container_content);
		ViewGroup containerProgress = (ViewGroup) mRootView.findViewById(R.id.container_progress);
		ViewGroup containerOffline = (ViewGroup) mRootView.findViewById(R.id.container_offline);
		ViewGroup containerEmpty = (ViewGroup) mRootView.findViewById(R.id.container_empty);
		containerContent.setVisibility(View.GONE);
		containerProgress.setVisibility(View.GONE);
		containerOffline.setVisibility(View.GONE);
		containerEmpty.setVisibility(View.VISIBLE);
		mViewState = ViewState.EMPTY;
	}


	private void renderView()
	{
        setupListeners();
	}

	private void findViews()
	{
		mSubjectET = (EditText) mRootView.findViewById(R.id.fragment_add_repair_subject);
		mDateET = (EditText) mRootView.findViewById(R.id.fragment_add_repair_et_date);
		mInfoET = (EditText) mRootView.findViewById(R.id.fragment_add_repair_info);
		mCarET = (EditText) mRootView.findViewById(R.id.fragment_add_repair_et_car);
		mCustomerET = (EditText) mRootView.findViewById(R.id.fragment_add_repair_et_customer);
		mPriceET = (EditText) mRootView.findViewById(R.id.fragment_add_repair_price);
		mMaintinanceTextView = (TextView) mRootView.findViewById(R.id.fragment_add_repair_maintinance_textView);
		mIsMaintinanceSwitchCompat = (SwitchCompat) mRootView.findViewById(R.id.fragment_add_repair_maintinance_switch);
		mDateTIL = (TextInputLayout) mRootView.findViewById(R.id.fragment_add_repair_til_date);
	}

	private void setupListeners()
	{
		mIsMaintinanceSwitchCompat.setOnCheckedChangeListener((buttonView, isChecked) ->
		{
            if(isChecked)
            {
                mMaintinanceTextView.setVisibility(View.VISIBLE);
            }
            else
            {
                mMaintinanceTextView.setVisibility(View.INVISIBLE);
            }
        });



		mDateET.setOnClickListener(v ->
		{
			Calendar now = Calendar.getInstance();
			DatePickerDialog dpd = DatePickerDialog.newInstance(
					AddRepairFragment.this,
					now.get(Calendar.YEAR),
					now.get(Calendar.MONTH),
					now.get(Calendar.DAY_OF_MONTH)
			);
			dpd.show(getFragmentManager() , "Datepickerdialog");
		});

		mCustomerET.setOnClickListener(v ->
		{
			startActivityForResult(SelectCustomerActivity.newIntent(getActivity()), 34);
		});
	}

	@Override
	public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth)
	{
		String date = dayOfMonth+"/"+(monthOfYear+1)+"/"+year;
		mDateET.setText(date);
		mInfoET.setFocusableInTouchMode(true);
		mInfoET.requestFocus();
	}
}
