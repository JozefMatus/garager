package com.beretis.garager.app.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.beretis.garager.app.Adapters.CarsCursorAdapter;
import com.beretis.garager.app.CustomersContentProvider;
import com.beretis.garager.app.R;
import com.beretis.garager.app.Tables.CustomerWithCarsViewTable;
import com.beretis.garager.app.Tables.CustomersTable;

import java.util.Date;


public class ConcreteCustomerFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>
{


    private int customersId;
    private ListView listView;
    private CarsCursorAdapter mAdapter;
    private Context context;
    private EditText name, email, phone;
    private static final String ARG_ADD = "add";
    private static final String ARG_EDIT = "edit";
    private static final String ARG_ID = "id";
    boolean add, edit;
    private Button save;


    private OnCarItemClickListener mListener;

    public static ConcreteCustomerFragment newInstance(boolean add, boolean edit, int id)
    {
        ConcreteCustomerFragment fragment = new ConcreteCustomerFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARG_EDIT, edit);
        args.putBoolean(ARG_ADD, add);
        args.putInt(ARG_ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    public ConcreteCustomerFragment()
    {
        // Required empty public constructor
    }

    private void getData()
    {
        getLoaderManager().initLoader(0, null, this);
        mAdapter = new CarsCursorAdapter(context, null, 0);
        listView.setAdapter(mAdapter);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            customersId = getArguments().getInt("id");
            add = getArguments().getBoolean(ARG_ADD);
            edit = getArguments().getBoolean(ARG_EDIT);
        }
        String peso = "";
        getLoaderManager().initLoader(1, null, this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_customer, container, false);
        name = (EditText) view.findViewById(R.id.fg_customer_et_name);
        email = (EditText) view.findViewById(R.id.fg_customer_et_email);
        phone = (EditText) view.findViewById(R.id.fg_customer_et_phone);
        listView = (ListView) view.findViewById(R.id.fg_customer_listview_cars);
        save = (Button) view.findViewById(R.id.fg_customer_add_customer);

        if(add == false)
        {
            getData();
            save.setVisibility(View.GONE);

        }
        else
        {
            save.setVisibility(View.VISIBLE);
            save.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(CustomersTable.CUSTOMERS_COLUMN_NAME ,name.getText().toString());
                    contentValues.put(CustomersTable.CUSTOMERS_COLUMN_PHONE ,phone.getText().toString());
                    contentValues.put(CustomersTable.CUSTOMERS_COLUMN_CREATE_DATE, new Date().getTime());
                    contentValues.put(CustomersTable.CUSTOMERS_COLUMN_LAST_VISIT, new Date().getTime());
                    getActivity().getContentResolver().insert(CustomersContentProvider.CONTENT_URI_CUSTOMERS, contentValues);
                    getActivity().finish();
                }
            });
        }
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Cursor selectedCarCursor = (Cursor) mAdapter.getCursor();
                int carId = selectedCarCursor.getInt(selectedCarCursor.getColumnIndex(CustomerWithCarsViewTable.COLUMN_CAR_ID));
                mListener.onCarItemClick(carId);
            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onItemClick(int carId)
    {
        if (mListener != null)
        {
            mListener.onCarItemClick(carId);
        }
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try
        {
            mListener = (OnCarItemClickListener) activity;
            context = activity.getApplicationContext();
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        mListener = null;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args)
    {

        if(id == 0)
        {
            Uri uri = ContentUris.withAppendedId(CustomersContentProvider.CONTENT_URI_CUSTOMER_WITH_CARS, customersId);//todo work to update this shit

            android.content.CursorLoader cursorLoader = new android.content.CursorLoader(getActivity(),
                    uri, null, null, null, CustomersContentProvider.PROVIDER_NAME);
            return cursorLoader;
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data)
    {
        String[] ddd = data.getColumnNames();
        int count = data.getCount();
        data.moveToFirst();
        name.setText(data.getString(data.getColumnIndex(CustomersTable.CUSTOMERS_COLUMN_NAME)));
        phone.setText(data.getString(data.getColumnIndex(CustomersTable.CUSTOMERS_COLUMN_PHONE)));
//        email.setText();
        mAdapter.swapCursor(data);
    }


    @Override
    public void onLoaderReset(Loader<Cursor> loader)
    {
        mAdapter.swapCursor(null);
    }


    public interface OnCarItemClickListener
    {
        // TODO: Update argument type and name
        public void onCarItemClick(int carId);
    }

}
