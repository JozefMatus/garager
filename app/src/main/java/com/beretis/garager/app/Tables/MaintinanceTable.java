package com.beretis.garager.app.Tables;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by jozefmatus on 12/25/14.
 */
public class MaintinanceTable
{
    public static final String TABLE_NAME = "maintinance";
    public static final String MAINTINANCE_COLUMN_ID = "_id";
    public static final String MAINTINANCE_COLUMN_TITLE = "title";
    public static final String MAINTINANCE_COLUMN_TIME_PERIOD = "time_period";
    public static final String MAINTINANCE_COLUMN_DISTANCE_PERIOD = "distance_preiod";


    static String DATABASE_CREATE = "CREATE TABLE " + TABLE_NAME + " (" +
            MAINTINANCE_COLUMN_ID + " INTEGER primary key autoincrement," +
            MAINTINANCE_COLUMN_TITLE + " TEXT NOT NULL," +
            MAINTINANCE_COLUMN_TIME_PERIOD + " INTEGER," +
            MAINTINANCE_COLUMN_DISTANCE_PERIOD + " INTEGER);";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(PartsTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(database);
    }
}
