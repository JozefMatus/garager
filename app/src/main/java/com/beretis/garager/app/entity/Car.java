package com.beretis.garager.app.entity;

import android.graphics.Bitmap;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: beretis
 * Date: 7/10/13
 * Time: 4:45 PM
 * To change this template use File | Settings | File Templates.
 */

@Parcel
public class Car
{
    private int id;
    private String factoryBrand;
    private String model;
    private int volume;
    private int power;
    private int year;
    private List<Repair> repairList;
    private Bitmap vehicleRegistration;
    private Bitmap carPhoto;
    private String vin;
    private String engineCode;

    public Car(String factoryBrand, String model, int volume, int power, int year, String vin, String engineCode)
    {
        this.factoryBrand = factoryBrand;
        this.model = model;
        this.volume = volume;
        this.power = power;
        this.year = year;
        this.vin = vin;
        this.engineCode = engineCode;
    }

    public int getId()
    {
        return id;
    }

    public String getFactoryBrand()
    {
        return factoryBrand;
    }

    public String getModel()
    {
        return model;
    }

    public int getVolume()
    {
        return volume;
    }

    public int getPower()
    {
        return power;
    }

    public int getYear()
    {
        return year;
    }

    public List<Repair> getRepairList()
    {
        return repairList;
    }

    public Bitmap getVehicleRegistration()
    {
        return vehicleRegistration;
    }

    public Bitmap getCarPhoto()
    {
        return carPhoto;
    }

    public void setFactoryBrand(String factoryBrand)
    {
        this.factoryBrand = factoryBrand;
    }

    public void setModel(String model)
    {
        this.model = model;
    }

    public void setVolume(int volume)
    {
        this.volume = volume;
    }

    public void setPower(int power)
    {
        this.power = power;
    }

    public void setYear(int year)
    {
        this.year = year;
    }

    public void setRepairList(List<Repair> repairList)
    {
        this.repairList = repairList;
    }

    public void setVehicleRegistration(Bitmap vehicleRegistration)
    {
        this.vehicleRegistration = vehicleRegistration;
    }

    public void setCarPhoto(Bitmap carPhoto)
    {
        this.carPhoto = carPhoto;
    }

    public String getVin()
    {
        return vin;
    }

    public String getEngineCode()
    {
        return engineCode;
    }

    public void setEngineCode(String engineCode)
    {
        this.engineCode = engineCode;
    }

    public void setVin(String vin)
    {
        this.vin = vin;
    }

}
