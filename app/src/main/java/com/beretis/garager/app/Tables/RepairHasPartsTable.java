package com.beretis.garager.app.Tables;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by beretis on 2.4.14.
 */
public class RepairHasPartsTable
{
    public static final String REPAIR_HAS_PARTS_TABLE_NAME = "repair_has_parts";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_REPAIR_ID = "repair_id";
    public static final String COLUMN_PART_ID = "part_id";
    public static final String COLUMN_PRICE = "price";
    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_WARRANTY_END_DATE = "warranty_end";


    static String DATABASE_CREATE = "CREATE TABLE " + REPAIR_HAS_PARTS_TABLE_NAME + " (" +
            COLUMN_ID + " INTEGER primary key autoincrement," +
            COLUMN_REPAIR_ID + " INTEGER NOT NULL," +
            COLUMN_PART_ID + "  REAL NOT NULL, " +
            COLUMN_PRICE + " REAL NOT NULL, " +
            COLUMN_DATE + " INTEGER NOT NULL, " +
            COLUMN_WARRANTY_END_DATE + " INTEGER NOT NULL, " +
            "FOREIGN KEY (" + COLUMN_REPAIR_ID + ") REFERENCES " + RepairsTable.REPAIRS_TABLE_NAME + "(" + RepairsTable.REPAIRS_COLUMN_ID + "), " +
            "FOREIGN KEY (" + COLUMN_PART_ID + ") REFERENCES " + PartsTable.PARTS_TABLE_NAME + "(" + PartsTable.PARTS_COLUMN_ID + "));";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(RepairHasPartsTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + REPAIR_HAS_PARTS_TABLE_NAME);
        onCreate(database);
    }
}
