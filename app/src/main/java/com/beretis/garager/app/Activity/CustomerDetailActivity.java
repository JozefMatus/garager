package com.beretis.garager.app.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.beretis.garager.app.Fragments.ConcreteCustomerFragment;
import com.beretis.garager.app.Fragments.Helper.Enums;
import com.beretis.garager.app.R;

public class CustomerDetailActivity extends AppCompatActivity implements ConcreteCustomerFragment.OnCarItemClickListener
{

    ConcreteCustomerFragment concreteCustomerFragment;
    int customerId;
    boolean edit, add;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer);
        setupActionBar();

        if(!getIntent().getExtras().isEmpty())
        {
            customerId = getIntent().getExtras().getInt("customerID");
            edit = getIntent().getExtras().getBoolean("edit");
            add = getIntent().getExtras().getBoolean("add");
        }


        concreteCustomerFragment = ConcreteCustomerFragment.newInstance(add, edit, customerId);
         getFragmentManager().beginTransaction().add(R.id.containeris, concreteCustomerFragment).commit();


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(!this.add)
        {
            getMenuInflater().inflate(R.menu.menu_customer, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings)
        {
            return true;
        }
        if(id == R.id.action_add_car)
        {
            Intent intent = new Intent(this, CarEditActivity.class);
            intent.putExtra("customerId", this.customerId);
            intent.putExtra("carId", -1);
            intent.putExtra("creationType", Enums.FragmentCreationType.EDIT.value);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCarItemClick(int carId)
    {
        Intent intent = new Intent(this, CarEditActivity.class);
        intent.putExtra("customerId", this.customerId);
        intent.putExtra("carId", carId);
        intent.putExtra("creationType", Enums.FragmentCreationType.PREVIEW.value);
        startActivity(intent);
    }

    private void setupActionBar()
    {
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        ActionBar bar = getSupportActionBar();
        bar.setDisplayUseLogoEnabled(false);
        bar.setDisplayShowTitleEnabled(true);
        bar.setDisplayShowHomeEnabled(true);
        bar.setDisplayHomeAsUpEnabled(false);
        bar.setHomeButtonEnabled(true);
    }
}
