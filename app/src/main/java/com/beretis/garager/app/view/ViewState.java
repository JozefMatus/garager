package com.beretis.garager.app.view;

/**
 * Created by jozefmatus on 25/07/15.
 */
public enum ViewState
{
	CONTENT, PROGRESS, OFFLINE, EMPTY
}