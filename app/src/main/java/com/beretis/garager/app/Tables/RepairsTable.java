package com.beretis.garager.app.Tables;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by beretis on 2.4.14.
 */
public class RepairsTable
{
    public static final String REPAIRS_TABLE_NAME = "repairs";
    public static final String REPAIRS_COLUMN_ID = "_id";
    public static final String REPAIRS_COLUMN_DATE = "date";
    public static final String REPAIRS_COLUMN_SUBJECT = "subject";
    public static final String REPAIRS_COLUMN_INFO = "info";
    public static final String REPAIRS_COLUMN_ISMAINTENANCE = "is_maintenance";
    public static final String REPAIRS_COLUMN_CAR = "car_id";
    public static final String REPAIRS_COLUMN_CUSTOMER = "customer_id";
    public static final String REPAIRS_COLUMN_PRICE = "price";
    public static final String REPAIRS_COLUMN_TIME_PERIOD = "time_period";//this is to notificate user that repair should be repeated
    public static final String REPAIRS_COLUMN_DISTANCE_PERIOD = "distance_preiod";

    static String DATABASE_CREATE = "CREATE TABLE " + REPAIRS_TABLE_NAME + " (" +
            REPAIRS_COLUMN_ID + " INTEGER primary key autoincrement," +
            REPAIRS_COLUMN_SUBJECT + " TEXT NOT NULL," +
            REPAIRS_COLUMN_INFO + " TEXT," +
            REPAIRS_COLUMN_ISMAINTENANCE + " BOOLEAN NOT NULL CHECK (" + REPAIRS_COLUMN_ISMAINTENANCE + " IN (0,1))," +
            REPAIRS_COLUMN_PRICE + " INTEGER NOT NULL," +
            REPAIRS_COLUMN_TIME_PERIOD + " INTEGER," +
            REPAIRS_COLUMN_DISTANCE_PERIOD + " INTEGER," +
            REPAIRS_COLUMN_DATE + " INTEGER NOT NULL," +
            REPAIRS_COLUMN_CAR + " INTEGER NOT NULL," +
            REPAIRS_COLUMN_CUSTOMER + " INTEGER NOT NULL," +
            "FOREIGN KEY (" + REPAIRS_COLUMN_CAR + ") REFERENCES " + CarsTable.CARS_TABLE_NAME + "(" + CarsTable.CARS_COLUMN_ID + "), " +
            "FOREIGN KEY (" + REPAIRS_COLUMN_CUSTOMER + ") REFERENCES " + CustomersTable.CUSTOMERS_TABLE_NAME + "(" + CustomersTable.CUSTOMERS_COLUMN_ID + "));";


    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(RepairsTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + REPAIRS_TABLE_NAME);
        onCreate(database);
    }
}
