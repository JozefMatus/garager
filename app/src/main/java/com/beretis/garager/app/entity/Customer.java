package com.beretis.garager.app.entity;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: beretis
 * Date: 7/10/13
 * Time: 4:05 PM
 * To change this template use File | Settings | File Templates.
 */
@Parcel
public class Customer
{
    private String name;
    private String phone;
    private String lastVisit;
    private String dateOfCreation;
    private String email;
    private int id;
    private List<Car> cars;

    public int getId()
    {
        return id;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getEmail()
    {
        return email;
    }

    public Customer(String name, String phone, List<Car> cars)
    {
        this.name = name;

        this.phone = phone;
        this.cars = cars;
    }

    public Customer(String name, String phone, String lastVisit, String dateOfCreation)
    {
        this.name = name;
        this.phone = phone;
        this.lastVisit = lastVisit;
        this.dateOfCreation = dateOfCreation;
    }

    public Customer(String name, String phone)
    {
        this.name = name;

        this.phone = phone;
        this.cars = new ArrayList<Car>();
    }

    public String getPhone()
    {
        return phone;
    }

    public List<Car> getCars()
    {
        return cars;
    }


    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public void setCars(List<Car> cars)
    {
        this.cars = cars;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getLastVisit()
    {
        return lastVisit;
    }

    public String getDateOfCreation()
    {
        return dateOfCreation;
    }

}
