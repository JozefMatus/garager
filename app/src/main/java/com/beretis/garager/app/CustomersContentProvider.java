package com.beretis.garager.app;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.database.SQLException;

import com.beretis.garager.app.Controller.MySqlLiteHelper;
import com.beretis.garager.app.Tables.CarsDocumentsPhotoTable;
import com.beretis.garager.app.Tables.CarsPhotoTable;
import com.beretis.garager.app.Tables.CarsTable;
import com.beretis.garager.app.Tables.CustomerWithCarsViewTable;
import com.beretis.garager.app.Tables.CustomersTable;
import com.beretis.garager.app.Tables.PartsTable;
import com.beretis.garager.app.Tables.RepairHasPartsTable;
import com.beretis.garager.app.Tables.RepairWithPartsViewTable;
import com.beretis.garager.app.Tables.RepairsTable;

import java.util.HashMap;

public class CustomersContentProvider extends ContentProvider
{

    private SQLiteDatabase database;

    // fields for my content provider
    public static final String PROVIDER_NAME = "com.beretis.garager.CustomersContentProvider";

    public static final String URL_CUSTOMERS = "content://" + PROVIDER_NAME + "/" + CustomersTable.CUSTOMERS_TABLE_NAME;
    public static final Uri CONTENT_URI_CUSTOMERS = Uri.parse(URL_CUSTOMERS);

    public static final String URL_CUSTOMER_WITH_CARS = "content://" + PROVIDER_NAME + "/" + CustomerWithCarsViewTable.TABLE_NAME;
    public static final Uri CONTENT_URI_CUSTOMER_WITH_CARS = Uri.parse(URL_CUSTOMER_WITH_CARS);

    public static final String URL_CARS = "content://" + PROVIDER_NAME + "/" + CarsTable.CARS_TABLE_NAME;
    public static final Uri CONTENT_URI_CARS = Uri.parse(URL_CARS);

    public static final String URL_REPAIRS = "content://" + PROVIDER_NAME + "/" + RepairsTable.REPAIRS_TABLE_NAME;
    public static final Uri CONTENT_URI_REPAIRS = Uri.parse(URL_REPAIRS);

    public static final String URL_PARTS = "content://" + PROVIDER_NAME + "/" + PartsTable.PARTS_TABLE_NAME;
    public static final Uri CONTENT_URI_PARTS = Uri.parse(URL_PARTS);

    public static final String URL_REPAIR_HAS_PARTS = "content://" + PROVIDER_NAME + "/" +
            RepairHasPartsTable.REPAIR_HAS_PARTS_TABLE_NAME;
    public static final Uri CONTENT_URI_REPAIR_HAS_PARTS = Uri.parse(URL_REPAIR_HAS_PARTS);

    public static final String URL_CARSPHOTOS = "content://" + PROVIDER_NAME + "/" + CarsPhotoTable.TABLE_NAME;
    public static final Uri CONTENT_URI_CARSPHOTOS = Uri.parse(URL_CARSPHOTOS);

    public static final String URL_CARSDOCUMENTSPHOTOS = "content://" + PROVIDER_NAME + "/" + CarsDocumentsPhotoTable.TABLE_NAME;
    public static final Uri CONTENT_URI_CARSDOCUMENTSPHOTOS = Uri.parse(URL_CARSDOCUMENTSPHOTOS);

    public static final String URL_REPAIR_WITH_PARTS = "content://" + PROVIDER_NAME + "/" + RepairWithPartsViewTable.TABLE_NAME;
    public static final Uri CONTENT_URI_REPAIR_WITH_PARTS = Uri.parse(URL_REPAIR_WITH_PARTS);


    // integer values used in content URI
    public static final int CUSTOMERS = 1;
    public static final int CUSTOMERS_ID = 2;


    public static final int CARS = 3;
    public static final int CARS_ID = 4;

    public static final int REPAIRS = 5;
    public static final int REPAIRS_ID = 6;

    public static final int PARTS = 7;
    public static final int PARTS_ID = 8;

    public static final int REPAIR_HAS_PARTS = 13;
    public static final int REPAIR_HAS_PARTS_ID = 14;

    public static final int CUSTOMER_WITH_CARS_ID = 15;

    public static final int CARSPHOTOS_ID = 16;

    public static final int CARSDOCUMENTSPHOTOS_ID = 17;

    public static final int REPAIR_WITH_PARTS_ID = 18;

    MySqlLiteHelper mySqlLiteHelper;

    // projection map for a query
    private static HashMap<String, String> CustomersMap;
    private static HashMap<String, String> CustomerWithCarsMap;
    private static HashMap<String, String> CarsMap;
    private static HashMap<String, String> RepairsMap;
    private static HashMap<String, String> PartsMap;
    private static HashMap<String, String> CustomerHasCarsMap;
    private static HashMap<String, String> CarHasRepairsMap;
    private static HashMap<String, String> RepairHasPartsMap;
    private static HashMap<String, String> CarsPhotosMap;
    private static HashMap<String, String> CarsDocumentsPhotosMap;
    private static HashMap<String, String> RepairWithPartsMap;




    // maps content URI "patterns" to the integer values that were set above
    static final UriMatcher uriMatcher;

    static
    {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, "customers", CUSTOMERS);
        uriMatcher.addURI(PROVIDER_NAME, "customers/#", CUSTOMERS_ID);
        uriMatcher.addURI(PROVIDER_NAME, CarsTable.CARS_TABLE_NAME + "/", CARS);
        uriMatcher.addURI(PROVIDER_NAME, CarsTable.CARS_TABLE_NAME + "/#", CARS_ID);
        uriMatcher.addURI(PROVIDER_NAME, RepairsTable.REPAIRS_TABLE_NAME + "/", REPAIRS);
        uriMatcher.addURI(PROVIDER_NAME, RepairsTable.REPAIRS_TABLE_NAME + "/#", REPAIRS_ID);
        uriMatcher.addURI(PROVIDER_NAME, PartsTable.PARTS_TABLE_NAME + "/", PARTS);
        uriMatcher.addURI(PROVIDER_NAME, PartsTable.PARTS_TABLE_NAME + "/#", PARTS_ID);
        uriMatcher.addURI(PROVIDER_NAME, RepairHasPartsTable.REPAIR_HAS_PARTS_TABLE_NAME + "/", REPAIR_HAS_PARTS);
        uriMatcher.addURI(PROVIDER_NAME, RepairHasPartsTable.REPAIR_HAS_PARTS_TABLE_NAME + "/#", REPAIR_HAS_PARTS_ID);
        uriMatcher.addURI(PROVIDER_NAME, CustomerWithCarsViewTable.TABLE_NAME + "/#", CUSTOMER_WITH_CARS_ID);
        uriMatcher.addURI(PROVIDER_NAME, CarsPhotoTable.TABLE_NAME + "/#", CARSPHOTOS_ID);
        uriMatcher.addURI(PROVIDER_NAME, CarsDocumentsPhotoTable.TABLE_NAME + "/#", CARSDOCUMENTSPHOTOS_ID);
        uriMatcher.addURI(PROVIDER_NAME, CarsPhotoTable.TABLE_NAME, CARSPHOTOS_ID);
        uriMatcher.addURI(PROVIDER_NAME, CarsDocumentsPhotoTable.TABLE_NAME, CARSDOCUMENTSPHOTOS_ID);
        uriMatcher.addURI(PROVIDER_NAME, RepairWithPartsViewTable.TABLE_NAME + "/#", REPAIR_WITH_PARTS_ID);
//        uriMatcher.addURI(PROVIDER_NAME, );
    }


    public CustomersContentProvider()
    {
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs)
    {
//        int count = 0;
//        switch (uriMatcher.match(uri))
//        {
//            case CUSTOMERS:
//            {
//                count = database.delete(CustomersTable.CUSTOMERS_TABLE_NAME, selection,
//                        selectionArgs);
//                break;
//            }
//            case CUSTOMERS_ID:
//            {
//                String id = uri.getLastPathSegment();
//                count = database.delete(CustomersTable.CUSTOMERS_TABLE_NAME, ID +
//                        " = " + uri.getLastPathSegment() +
//                        (!TextUtils.isEmpty(selection) ? " AND (" +
//                                selection + ')' : ""), selectionArgs);
//
//            }
//            default:
//            {
//                throw new IllegalArgumentException("Unsupported URI " + uri);
//            }
//        }
//        getContext().getContentResolver().notifyChange(uri, null);
//        return count;
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public String getType(Uri uri)
    {
        // TODO: Implement this to handle requests for the MIME type of the data




        switch (uriMatcher.match(uri))
        {
            case CUSTOMERS:
                return "vnd.android.cursor.dir/vnd.com.beretis.garager.app.customers";
            case CUSTOMERS_ID:
                return "vnd.android.cursor.item/vnd.com.beretis.garager.app.customers";
            case CARS:
                return "vnd.android.cursor.dir/vnd.com.beretis.garager.app." + CarsTable.CARS_TABLE_NAME;
            case CARS_ID:
                return "vnd.android.cursor.item/vnd.com.beretis.garager.app." + CarsTable.CARS_TABLE_NAME;
            case REPAIRS:
                return "vnd.android.cursor.dir/vnd.com.beretis.garager.app." + RepairsTable.REPAIRS_TABLE_NAME;
            case REPAIRS_ID:
                return "vnd.android.cursor.item/vnd.com.beretis.garager.app." + RepairsTable.REPAIRS_TABLE_NAME;
            case PARTS:
                return "vnd.android.cursor.dir/vnd.com.beretis.garager.app." + PartsTable.PARTS_TABLE_NAME;
            case PARTS_ID:
                return "vnd.android.cursor.item/vnd.com.beretis.garager.app." + PartsTable.PARTS_TABLE_NAME;
            case CUSTOMER_WITH_CARS_ID:
                return "vnd.android.cursor.dir/vnd.com.beretis.garager.app." + CustomerWithCarsViewTable.TABLE_NAME;
            case REPAIR_HAS_PARTS:
                return "vnd.android.cursor.dir/vnd.com.beretis.garager.app." + RepairHasPartsTable.REPAIR_HAS_PARTS_TABLE_NAME;
            case REPAIR_HAS_PARTS_ID:
                return "vnd.android.cursor.item/vnd.com.beretis.garager.app." + RepairHasPartsTable.REPAIR_HAS_PARTS_TABLE_NAME;
            case CARSPHOTOS_ID:
                return "vnd.android.cursor.item/vnd.com.beretis.garager.app." + CarsPhotoTable.TABLE_NAME;
            case CARSDOCUMENTSPHOTOS_ID:
                return "vnd.android.cursor.item/vnd.com.beretis.garager.app." + CarsDocumentsPhotoTable.TABLE_NAME;
            case REPAIR_WITH_PARTS_ID:
                return "vnd.android.cursor.dir/vnd.com.beretis.garager.app." + RepairWithPartsViewTable.TABLE_NAME;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

    }

    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        Uri _uri = null;
        switch (uriMatcher.match(uri))
        {
            case CUSTOMERS:
                long customerRow = database.insert(CustomersTable.CUSTOMERS_TABLE_NAME, "", values);
                //---if added successfully---
                if (customerRow > 0)
                {
                    _uri = ContentUris.withAppendedId(CONTENT_URI_CUSTOMERS, customerRow);
                    getContext().getContentResolver().notifyChange(_uri, null);
                }
                break;
            case CARS:
                long carRow = database.insert(CarsTable.CARS_TABLE_NAME, "", values);
                //---if added successfully---
                if (carRow > 0)
                {
                    _uri = ContentUris.withAppendedId(CustomersContentProvider.CONTENT_URI_CUSTOMER_WITH_CARS, carRow);
//                    getContext().getContentResolver().notifyChange(_uri, null);
//                    getContext().getContentResolver().notifyChange(_uri, null);
                }
                break;
            case REPAIRS:
                long repairRow = database.insert(RepairsTable.REPAIRS_TABLE_NAME, "", values);
                //---if added successfully---
                if (repairRow > 0)
                {
                    _uri = ContentUris.withAppendedId(CONTENT_URI_REPAIRS, repairRow);
//                    getContext().getContentResolver().notifyChange(_uri, null);
                }
                break;
            case PARTS:
                long partRow = database.insert(PartsTable.PARTS_TABLE_NAME, "", values);
                //---if added successfully---
                if (partRow > 0)
                {
                    _uri = ContentUris.withAppendedId(CONTENT_URI_PARTS, partRow);
//                    getContext().getContentResolver().notifyChange(_uri, null);
                }
                break;
            case REPAIR_HAS_PARTS:
            long repairHasPartRow = database.insert(RepairHasPartsTable.REPAIR_HAS_PARTS_TABLE_NAME, "", values);
            //---if added successfully---
            if (repairHasPartRow > 0)
            {
                _uri = ContentUris.withAppendedId(CONTENT_URI_REPAIR_HAS_PARTS, repairHasPartRow);
//                    getContext().getContentResolver().notifyChange(_uri, null);
            }
            break;
            case CARSPHOTOS_ID:
                long carphoto = database.insert(CarsPhotoTable.TABLE_NAME, "", values);
                //---if added successfully---
                if (carphoto > 0)
                {
                    _uri = ContentUris.withAppendedId(CONTENT_URI_REPAIR_HAS_PARTS, carphoto);
//                    getContext().getContentResolver().notifyChange(_uri, null);
                }
                break;
            case CARSDOCUMENTSPHOTOS_ID:
                long carDocumentPhoto = database.insert(CarsDocumentsPhotoTable.TABLE_NAME, "", values);
                //---if added successfully---
                if (carDocumentPhoto > 0)
                {
                    _uri = ContentUris.withAppendedId(CONTENT_URI_REPAIR_HAS_PARTS, carDocumentPhoto);
//                    getContext().getContentResolver().notifyChange(_uri, null);
                }
                break;
            default:
                throw new SQLException("Failed to insert row into " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null, false);
        return _uri;
    }

    @Override
    public boolean onCreate()
    {
        // TODO: Implement this to initialize your content provider on startup.
        Context context = getContext();
        mySqlLiteHelper = new MySqlLiteHelper(context);
        database = mySqlLiteHelper.getWritableDatabase();
        if (database == null)
            return false;
        else
            return true;

    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder)
    {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        // the TABLE_NAME to query on

        int test = uriMatcher.match(ContentUris.withAppendedId(CONTENT_URI_CUSTOMER_WITH_CARS,2));


        switch (uriMatcher.match(uri))
        {
            // maps all database column names
            case CUSTOMERS:
                queryBuilder.setTables(CustomersTable.CUSTOMERS_TABLE_NAME);
                queryBuilder.setProjectionMap(CustomersMap);
                break;
            case CUSTOMERS_ID:
                queryBuilder.setTables(CustomersTable.CUSTOMERS_TABLE_NAME);
                queryBuilder.appendWhere(CustomersTable.CUSTOMERS_COLUMN_ID + "=" + uri.getLastPathSegment());
                break;
            case CARS:
                queryBuilder.setTables(CarsTable.CARS_TABLE_NAME);
                queryBuilder.setProjectionMap(CarsMap);
                queryBuilder.appendWhere(CarsTable.CARS_COLUMN_CUSTOMER_ID + "=" + uri.getLastPathSegment());
                break;
            case CARS_ID:
                queryBuilder.setTables(CarsTable.CARS_TABLE_NAME);
                queryBuilder.appendWhere(CarsTable.CARS_COLUMN_ID + "=" + uri.getLastPathSegment());
                break;
            case REPAIRS:
                queryBuilder.setTables(RepairsTable.REPAIRS_TABLE_NAME);
                queryBuilder.setProjectionMap(RepairsMap);
                break;
            case REPAIRS_ID:
                queryBuilder.setTables(RepairsTable.REPAIRS_TABLE_NAME);
                queryBuilder.appendWhere(RepairsTable.REPAIRS_COLUMN_ID + "=" + uri.getLastPathSegment());
                break;
            case PARTS:
                queryBuilder.setTables(PartsTable.PARTS_TABLE_NAME);
                queryBuilder.setProjectionMap(PartsMap);
                break;
            case PARTS_ID:
                queryBuilder.setTables(PartsTable.PARTS_TABLE_NAME);
                queryBuilder.appendWhere(PartsTable.PARTS_COLUMN_ID + "=" + uri.getLastPathSegment());
                break;
            case REPAIR_HAS_PARTS:
                queryBuilder.setTables(RepairHasPartsTable.REPAIR_HAS_PARTS_TABLE_NAME);
                queryBuilder.setProjectionMap(RepairHasPartsMap);
                break;
            case REPAIR_HAS_PARTS_ID:
                queryBuilder.setTables(RepairHasPartsTable.REPAIR_HAS_PARTS_TABLE_NAME);
                queryBuilder.appendWhere(RepairHasPartsTable.COLUMN_ID + "=" + uri.getLastPathSegment());
                break;
            case CUSTOMER_WITH_CARS_ID:
                queryBuilder.setTables(CustomerWithCarsViewTable.TABLE_NAME);
                queryBuilder.appendWhere(CustomerWithCarsViewTable.COLUMN_ID + "=" + uri.getLastPathSegment());
                queryBuilder.setProjectionMap(CustomerWithCarsMap);
                break;
            case CARSPHOTOS_ID:
                queryBuilder.setTables(CarsPhotoTable.TABLE_NAME);
                queryBuilder.appendWhere(CarsPhotoTable.CARSPHOTO_COLUMN_CAR_ID + "=" + uri.getLastPathSegment());
                queryBuilder.setProjectionMap(CarsPhotosMap);
                break;
            case CARSDOCUMENTSPHOTOS_ID:
                queryBuilder.setTables(CarsDocumentsPhotoTable.TABLE_NAME);
                queryBuilder.appendWhere(CarsDocumentsPhotoTable.CARSDOMUENTSPHOTO_COLUMN_CAR_ID + "=" + uri.getLastPathSegment());
                queryBuilder.setProjectionMap(CarsDocumentsPhotosMap);
                break;
            case REPAIR_WITH_PARTS_ID:
                queryBuilder.setTables(RepairWithPartsViewTable.TABLE_NAME);
                queryBuilder.appendWhere(RepairWithPartsViewTable.COLUMN_ID + "=" + uri.getLastPathSegment());
                queryBuilder.setProjectionMap(RepairWithPartsMap);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
//        if (sortOrder == null || sortOrder == ""){
//            // No sorting-> sort on names by default
//            sortOrder = NAME;
//        }
         Cursor cursor = queryBuilder.query(database, projection, selection,
                selectionArgs, null, null, null);
        /**
         124
         * register to watch a content URI for changes
         125
         */
        if(uriMatcher.match(uri) == CUSTOMER_WITH_CARS_ID)
        {
            cursor.setNotificationUri(getContext().getContentResolver(), CONTENT_URI_CARS);
        }
        else if(uriMatcher.match(uri) == REPAIR_WITH_PARTS_ID)
        {
            cursor.setNotificationUri(getContext().getContentResolver(), CONTENT_URI_PARTS);
        }
        else
        {
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        }
        return cursor;

    }

    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs)
    {
        // TODO: Implement this to handle requests to update one or more rows.
//        int count = 0;
//        switch (uriMatcher.match(uri))
//        {
//            case CUSTOMERS:
//            {
//                count = database.update(CustomersTable.CUSTOMERS_TABLE_NAME, values, selection, selectionArgs);
//                break;
//            }
//            case CUSTOMERS_ID:
//            {
//                String id = uri.getLastPathSegment();
//                count = database.update(CustomersTable.CUSTOMERS_TABLE_NAME, values, ID +
//                        " = " + uri.getLastPathSegment() +
//                (!TextUtils.isEmpty(selection) ? " AND (" +
//                selection + ')' : ""), selectionArgs);
//
//            }
//            default:
//            {
//                throw new IllegalArgumentException("Unsupported URI " + uri);
//            }
//        }
//        getContext().getContentResolver().notifyChange(uri, null);
//        return count;
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
