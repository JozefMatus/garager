package com.beretis.garager.app.Fragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.beretis.garager.app.Fragments.Helper.Enums;
import com.beretis.garager.app.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RepairDetailFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RepairDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RepairDetailFragment extends Fragment
{
    private static final String ARG_CREATION_TYPE = "creationType";
    private static final String ARG_ID = "id";
    private OnFragmentInteractionListener mListener;
    private Enums.FragmentCreationType mCreationType;
    private int id;

    public static RepairDetailFragment newInstance(int repairId, boolean edit)
    {
        RepairDetailFragment fragment = new RepairDetailFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARG_CREATION_TYPE, edit);
        args.putInt(ARG_ID, repairId);
        fragment.setArguments(args);
        return fragment;
    }

    public RepairDetailFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            id = getArguments().getInt(ARG_ID);
            mCreationType = Enums.FragmentCreationType.fromInt(getArguments().getInt(ARG_CREATION_TYPE));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_repair_detail, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri)
    {
        if (mListener != null)
        {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
//        try
//        {
//            mListener = (OnFragmentInteractionListener) activity;
//        }
//        catch (ClassCastException e)
//        {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener
    {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

}
