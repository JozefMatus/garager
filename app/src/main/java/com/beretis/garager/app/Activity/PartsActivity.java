package com.beretis.garager.app.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;

import com.beretis.garager.app.Fragments.CustomersFragment;
import com.beretis.garager.app.R;


public class PartsActivity extends Activity
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_customers);
		getFragmentManager().beginTransaction().add(R.id.containeris, CustomersFragment.newInstance("", "")).commit();

	}

	public static Intent newIntent(Context context)
	{
		Intent intent = new Intent(context, RepairsActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		return intent;
	}

}
