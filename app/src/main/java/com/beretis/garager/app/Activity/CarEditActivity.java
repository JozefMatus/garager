package com.beretis.garager.app.Activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.beretis.garager.app.Fragments.CarEditFragment;
import com.beretis.garager.app.Fragments.Helper.Enums;
import com.beretis.garager.app.R;

public class CarEditActivity extends AppCompatActivity
{

    private int customerId;
    private int carId;
    private Enums.FragmentCreationType mCreationType;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_car);
        setupActionBar();
        customerId = getIntent().getExtras().getInt("customerId");
        carId = getIntent().getExtras().getInt("carId");
        int blabla= getIntent().getExtras().getInt("creationType");
        mCreationType = Enums.FragmentCreationType.fromInt(getIntent().getExtras().getInt("creationType"));
        if (savedInstanceState == null)
        {

            getFragmentManager().beginTransaction()
                    .add(R.id.containeris, CarEditFragment.newInstance(mCreationType, customerId, carId))
                    .commit();
        }
    }


    private void setupActionBar()
    {
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        ActionBar bar = getSupportActionBar();
        bar.setDisplayUseLogoEnabled(false);
        bar.setDisplayShowTitleEnabled(true);
        bar.setDisplayShowHomeEnabled(true);
        bar.setDisplayHomeAsUpEnabled(false);
        bar.setHomeButtonEnabled(true);
    }

}
