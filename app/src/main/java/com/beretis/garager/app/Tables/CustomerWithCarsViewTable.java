package com.beretis.garager.app.Tables;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by jozefmatus on 1/3/15.
 */
public class CustomerWithCarsViewTable
{

    public static final String TABLE_NAME = "customerWithCarsView";

    public static final String COLUMN_ID = CustomersTable.CUSTOMERS_COLUMN_ID;
    public static final String COLUMN_CAR_ID = "car_id";

    private static final String DATABASE_CREATE = ""
            + "CREATE VIEW " + TABLE_NAME
            + " AS SELECT " + CustomersTable.CUSTOMERS_TABLE_NAME + ".*"
            + ", " + CarsTable.CARS_TABLE_NAME + "." + CarsTable.CARS_COLUMN_ID + " AS car_id "
            + ", " + CarsTable.CARS_TABLE_NAME + "." + CarsTable.CARS_COLUMN_BRAND
            + ", " + CarsTable.CARS_TABLE_NAME + "." + CarsTable.CARS_COLUMN_MODEL
            + " FROM " + CustomersTable.CUSTOMERS_TABLE_NAME + " LEFT JOIN " + CarsTable.CARS_TABLE_NAME
            + " ON " + CustomersTable.CUSTOMERS_TABLE_NAME + "." + CustomersTable.CUSTOMERS_COLUMN_ID
            + " = " + CarsTable.CARS_TABLE_NAME + "." + CarsTable.CARS_COLUMN_CUSTOMER_ID;



    public static void onCreate(SQLiteDatabase database)
    {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(CustomerWithCarsViewTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP VIEW IF EXISTS " + TABLE_NAME);
        onCreate(database);
    }

}
