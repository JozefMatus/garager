package com.beretis.garager.app.Fragments.Helper;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jozefmatus on 25/03/15.
 */
public class Enums
{

    public enum FragmentCreationType
    {
        ADD(1),
        EDIT(2),
        PREVIEW(3);
        public int value;

        FragmentCreationType(int mValue)
        {
            this.value = mValue;
        }

        private static final Map<Integer, FragmentCreationType> intToTypeMap = new HashMap<Integer, FragmentCreationType>();

        static
        {
            for (FragmentCreationType type : FragmentCreationType.values())
            {
                intToTypeMap.put(type.value, type);
            }
        }

        public static FragmentCreationType fromInt(int i)
        {
            FragmentCreationType type = intToTypeMap.get(Integer.valueOf(i));
            if (type == null)
            {
                return FragmentCreationType.PREVIEW;
            }
            return type;
        }
    }
}
