package com.beretis.garager.app.Tables;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by beretis on 2.4.14.
 */
public class CustomersTable
{
    public static final String CUSTOMERS_TABLE_NAME = "customers";
    public static final String CUSTOMERS_COLUMN_ID = "_id";
    public static final String CUSTOMERS_COLUMN_NAME = "customer_name";
    public static final String CUSTOMERS_COLUMN_PHONE = "customer_phone";
    public static final String CUSTOMERS_COLUMN_CREATE_DATE = "customer_create_date";
    public static final String CUSTOMERS_COLUMN_LAST_VISIT = "customer_last_visit_date";

    static String DATABASE_CREATE = "CREATE TABLE " + CUSTOMERS_TABLE_NAME + " (" +
            CUSTOMERS_COLUMN_ID + " INTEGER primary key autoincrement," +
            CUSTOMERS_COLUMN_NAME + " TEXT NOT NULL," +
            CUSTOMERS_COLUMN_PHONE + " TEXT NOT NULL," +
            CUSTOMERS_COLUMN_CREATE_DATE + " INTEGER," +
            CUSTOMERS_COLUMN_LAST_VISIT + " INTEGER NOT NULL);";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(CustomersTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + CUSTOMERS_TABLE_NAME);
        onCreate(database);
    }

}
