package com.beretis.garager.app.Services;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;

import com.beretis.garager.app.R;

import java.util.Arrays;
import java.util.List;


/**
 * Created by jozefmatus on 26/01/16.
 */
public class ColorPicker
{

	static List<Integer> MATERIAL = Arrays.asList(
				0xffe57373,
				0xfff06292,
				0xffba68c8,
				0xff9575cd,
				0xff7986cb,
				0xff64b5f6,
				0xff4fc3f7,
				0xff4dd0e1,
				0xff4db6ac,
				0xff81c784,
				0xffaed581,
				0xffff8a65,
				0xffd4e157,
				0xffffd54f,
				0xffffb74d,
				0xffa1887f,
				0xff90a4ae);


	public static int getColorFromInt(int i)
	{
		int number = Math.abs(i) % MATERIAL.size();
		return MATERIAL.get(number);
	}
}
