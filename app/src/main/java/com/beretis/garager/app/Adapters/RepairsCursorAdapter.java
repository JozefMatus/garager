package com.beretis.garager.app.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.beretis.garager.app.R;
import com.beretis.garager.app.Tables.PartsTable;
import com.beretis.garager.app.Tables.RepairsTable;

/**
 * Created by jozefmatus on 6/11/15.
 */
public class RepairsCursorAdapter extends CursorAdapter
{
    LayoutInflater mInflater;

    public RepairsCursorAdapter(Context context, Cursor c, boolean autoRequery)
    {
        super(context, c, autoRequery);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public RepairsCursorAdapter(Context context, Cursor c, int flags)
    {
        super(context, c, flags);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup)
    {

        View view = mInflater.inflate(R.layout.li_repair,null);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor)
    {
        TextView title = (TextView) view.findViewById(R.id.li_repair_subject);
        ImageView image = (ImageView) view.findViewById(R.id.li_repair_image);



        title.setText(cursor.getString(cursor.getColumnIndex(RepairsTable.REPAIRS_COLUMN_SUBJECT)));
    }
}
