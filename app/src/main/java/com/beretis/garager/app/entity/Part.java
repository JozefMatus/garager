package com.beretis.garager.app.entity;

import org.parceler.Parcel;


/**
 * Created with IntelliJ IDEA.
 * User: beretis
 * Date: 7/10/13
 * Time: 7:19 PM
 * To change this template use File | Settings | File Templates.
 */
@Parcel
public class Part
{
    int purchizePrize;
    int sellingPrize;
    int barecode;
    String title;
    String supplier;

    public Part(int sellingPrize, int purchizePrize, String title, String supplier)
    {
        this.sellingPrize = sellingPrize;
        this.purchizePrize = purchizePrize;
        this.title = title;
        this.supplier = supplier;
    }

    public int getPurchizePrize()
    {
        return purchizePrize;
    }

    public int getSellingPrize()
    {
        return sellingPrize;
    }

    public int getBarecode()
    {
        return barecode;
    }

    public String getTitle()
    {
        return title;
    }

    public String getSupplier()
    {
        return supplier;
    }

    public void setPurchizePrize(int purchizePrize)
    {
        this.purchizePrize = purchizePrize;
    }

    public void setSellingPrize(int sellingPrize)
    {
        this.sellingPrize = sellingPrize;
    }

    public void setBarecode(int barecode)
    {
        this.barecode = barecode;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public void setSupplier(String supplier)
    {
        this.supplier = supplier;
    }

}
