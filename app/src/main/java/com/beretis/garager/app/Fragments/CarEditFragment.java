package com.beretis.garager.app.Fragments;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.Toast;

import com.beretis.garager.app.CustomersContentProvider;
import com.beretis.garager.app.Fragments.Helper.Enums;
import com.beretis.garager.app.MyFileContentProvider;
import com.beretis.garager.app.R;
import com.beretis.garager.app.Tables.CarsPhotoTable;
import com.beretis.garager.app.Tables.CarsTable;
import com.bumptech.glide.Glide;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;


import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CarEditFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CarEditFragment#newInstance} factory method to
 * create an instance of this fragment.
 */

public class CarEditFragment extends Fragment implements DatePickerDialog.OnDateSetListener, LoaderManager.LoaderCallbacks<Cursor>//todo on state change
{
	public static final int LOADER_PHOTOS_ID = 1;
	public static final int MY_PERMISSIONS_REQUEST_CAMERA = 245;

	public static Double[] volumes;

	private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
	private static final String ARG_CREATION_TYPE = "creationType";
	private static final String ARG_CUSTOMER_ID = "customerId";
	private static final String ARG_CAR_ID = "carId";
	private final int CAMERA_RESULT = 1;

	private final String Tag = getClass().getName();

	private List<String> photoPaths = new ArrayList<>();
	private AutoCompleteTextView brandET, modelET, dateOfFirstRegistrationET, engineVolumeET, enginePowerET, engineCodeET, vinCodeET;
	private ViewPager photoPager;
	private PagerAdapter mPagerAdapter;
	private FloatingActionButton newImageButton;


	private Enums.FragmentCreationType mCreationType;
	private int customerId, carId;

	private OnFragmentInteractionListener mListener;


	public static CarEditFragment newInstance(Enums.FragmentCreationType creationType, int mCustomerId, int mCarId)
	{
		CarEditFragment fragment = new CarEditFragment();
		Bundle args = new Bundle();
		args.putInt(ARG_CREATION_TYPE, creationType.value);
		args.putInt(ARG_CUSTOMER_ID, mCustomerId);
		args.putInt(ARG_CAR_ID, mCarId);
		fragment.setArguments(args);
		return fragment;
	}


	public CarEditFragment()
	{
		// Required empty public constructor
	}


	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		if(getArguments() != null)
		{
			mCreationType = Enums.FragmentCreationType.fromInt((getArguments().getInt(ARG_CREATION_TYPE)));
			customerId = getArguments().getInt(ARG_CUSTOMER_ID);
			carId = getArguments().getInt(ARG_CAR_ID);
		}
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState)
	{
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.fragment_edit_car, container, false);
		findElements(view);
		setListeners();
		setup();
		adaptFragmnetBasedOnCreationMode();
		assainAdapters();
		setHasOptionsMenu(true);
//		setHasOptionsMenu(this.mCreationType == Enums.FragmentCreationType.ADD);

		return view;
	}


	// TODO: Rename method, update argument and hook method into UI event
	public void onButtonPressed(Uri uri)
	{
		if(mListener != null)
		{
			mListener.onFragmentInteraction(uri);
		}
	}


	private void findElements(View view)
	{
		brandET = (AutoCompleteTextView) view.findViewById(R.id.fg_add_car_et_brand);
		modelET = (AutoCompleteTextView) view.findViewById(R.id.fg_add_car_et_model);
		dateOfFirstRegistrationET = (AutoCompleteTextView) view.findViewById(R.id.fg_add_car_et_year);
		engineVolumeET = (AutoCompleteTextView) view.findViewById(R.id.fg_add_car_et_engine_volume);
		enginePowerET = (AutoCompleteTextView) view.findViewById(R.id.fg_add_car_et_engine_power);
		engineCodeET = (AutoCompleteTextView) view.findViewById(R.id.fg_add_car_et_engine_code);
		vinCodeET = (AutoCompleteTextView) view.findViewById(R.id.fg_add_car_et_vin);
		photoPager = (ViewPager) view.findViewById(R.id.fg_edit_car_viewpager);
		newImageButton = (FloatingActionButton) view.findViewById(R.id.fg_edit_car_addPhoto);
		newImageButton.setOnClickListener(new View.OnClickListener()
		{
			public void onClick(View v)
			{
				addNewPhoto();
			}
		});
	}

	private void setup()
	{
		mPagerAdapter = new CarPhotoPagerAdapter();
		setVolumeAdapter();
		dateOfFirstRegistrationET.setOnClickListener(v ->
		{
			Calendar now = Calendar.getInstance();
			DatePickerDialog dpd = DatePickerDialog.newInstance(
					CarEditFragment.this,
					now.get(Calendar.YEAR),
					now.get(Calendar.MONTH),
					now.get(Calendar.DAY_OF_MONTH)
			);
			dpd.show(getFragmentManager(), "Datepickerdialog");
		});
	}


	private void setListeners()
	{

	}


	private void assainAdapters()
	{
		this.photoPager.setAdapter(new CarPhotoPagerAdapter());
	}


	private void adaptFragmnetBasedOnCreationMode()
	{
		brandET.setEnabled(mCreationType == Enums.FragmentCreationType.EDIT);
		modelET.setEnabled(mCreationType == Enums.FragmentCreationType.EDIT);
		dateOfFirstRegistrationET.setEnabled(mCreationType == Enums.FragmentCreationType.EDIT);
		engineVolumeET.setEnabled(mCreationType == Enums.FragmentCreationType.EDIT);
		enginePowerET.setEnabled(mCreationType == Enums.FragmentCreationType.EDIT);
		engineCodeET.setEnabled(mCreationType == Enums.FragmentCreationType.EDIT);
		vinCodeET.setEnabled(mCreationType == Enums.FragmentCreationType.EDIT);

		newImageButton.setVisibility(mCreationType == Enums.FragmentCreationType.EDIT ? View.VISIBLE : View.GONE);

		getActivity().invalidateOptionsMenu();
	}


	@Override
	public void onStart()
	{
		if(this.mCreationType == Enums.FragmentCreationType.PREVIEW)
		{
			getActivity().getLoaderManager().initLoader(0, null, this);
			getActivity().getLoaderManager().initLoader(LOADER_PHOTOS_ID, null, this);

		}
		super.onStart();
	}


	@Override
	public void onAttach(Context context)
	{
		super.onAttach(context);
		Activity a;

		if (context instanceof Activity){
			a=(Activity) context;
			try
			{
				mListener = (OnFragmentInteractionListener) a;
			}
			catch(ClassCastException e)
			{
//				throw new ClassCastException(a.toString()
//                                + " must implement OnFragmentInteractionListener");
			}
		}

	}


	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		super.onCreateOptionsMenu(menu, inflater);
		switch (mCreationType)
		{
			case EDIT:
				getActivity().getMenuInflater().inflate(R.menu.menu_car_edit, menu);
				return;
			case PREVIEW:
				getActivity().getMenuInflater().inflate(R.menu.menu_car_detail, menu);
				return;
			default:
				getActivity().getMenuInflater().inflate(R.menu.menu_car_detail, menu);
				return;
		}

	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_car_add)
		{
			this.saveCarToDB();
			getActivity().finish();
			return true;
		}
		else if (id == R.id.action_car_edit)
		{
			mCreationType = Enums.FragmentCreationType.EDIT;
			adaptFragmnetBasedOnCreationMode();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}


	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
	}


	@Override
	public void onDetach()
	{
		super.onDetach();
		mListener = null;
	}


	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args)
	{

		if(id == 0)
		{
			Uri uri = ContentUris.withAppendedId(CustomersContentProvider.CONTENT_URI_CARS, carId);//todo work to update this shit

			android.content.CursorLoader cursorLoader = new android.content.CursorLoader(getActivity(),
					uri, null, null, null, CustomersContentProvider.PROVIDER_NAME);
			return cursorLoader;
		}
		else if(id == LOADER_PHOTOS_ID)
		{
			Uri uri = ContentUris.withAppendedId(CustomersContentProvider.CONTENT_URI_CARSPHOTOS, carId);

			android.content.CursorLoader cursorLoader = new CursorLoader(getActivity(), uri, null, null, null, null);
			return cursorLoader;
		}
		return null;
	}


	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data)
	{
		switch(loader.getId())
		{
			case 0:
				data.moveToFirst();
				brandET.setText(data.getString(data.getColumnIndex(CarsTable.CARS_COLUMN_BRAND)));
				modelET.setText(data.getString(data.getColumnIndex(CarsTable.CARS_COLUMN_MODEL)));
				dateOfFirstRegistrationET.setText(data.getString(data.getColumnIndex(CarsTable.CARS_COLUMN_YEAR)));
				engineVolumeET.setText(data.getString(data.getColumnIndex(CarsTable.CARS_COLUMN_VOLUME)));
				enginePowerET.setText(data.getString(data.getColumnIndex(CarsTable.CARS_COLUMN_POWER)));
				engineCodeET.setText(data.getString(data.getColumnIndex(CarsTable.CARS_COLUMN_ENGINE_CODE)));
				vinCodeET.setText(data.getString(data.getColumnIndex(CarsTable.CARS_COLUMN_VIN)));
				break;
			case LOADER_PHOTOS_ID:
				ArrayList<String> photos = new ArrayList<>();
				while(data.moveToNext())
				{
					photos.add(data.getString(data.getColumnIndex(CarsPhotoTable.CARSPHOTO_COLUMN_PATH)));
				}
				photoPaths = photos;
				photoPager.setAdapter(mPagerAdapter);
				mPagerAdapter.notifyDataSetChanged();
				break;
			case 2:
				// do some more stuff here
				break;
			default:
				break;
		}

	}


	private void addNewPhoto()
	{
		if(!getPermisionStatus())
		{
			requestPermissions();
		}
		Intent i = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		i.putExtra(MediaStore.EXTRA_OUTPUT, MyFileContentProvider.CONTENT_URI);
		i.putExtra(MediaStore.EXTRA_SCREEN_ORIENTATION, ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		startActivityForResult(i, CAMERA_RESULT);

	}


	private void saveCarToDB()
	{
		ContentValues contentValues = new ContentValues();
		contentValues.put(CarsTable.CARS_COLUMN_BRAND, brandET.getText().toString());
		contentValues.put(CarsTable.CARS_COLUMN_MODEL, modelET.getText().toString());
		contentValues.put(CarsTable.CARS_COLUMN_YEAR, Integer.valueOf(dateOfFirstRegistrationET.getText().toString()));
		contentValues.put(CarsTable.CARS_COLUMN_VOLUME, Double.valueOf(engineVolumeET.getText().toString()));
		contentValues.put(CarsTable.CARS_COLUMN_POWER, Double.valueOf(engineVolumeET.getText().toString()));
		contentValues.put(CarsTable.CARS_COLUMN_ENGINE_CODE, engineCodeET.getText().toString());
		contentValues.put(CarsTable.CARS_COLUMN_VIN, vinCodeET.getText().toString());
		contentValues.put(CarsTable.CARS_COLUMN_CUSTOMER_ID, customerId);
		Uri uri = getActivity().getContentResolver().insert(CustomersContentProvider.CONTENT_URI_CARS, contentValues);
		this.carId = Integer.valueOf(uri.getPathSegments().get(uri.getPathSegments().size() -1));
		for (String path : photoPaths)
		{
			saveImageToDb(path);
		}

	}

	private void saveImageToDb(String photoPath)
	{
		ContentValues contentValues = new ContentValues();
		contentValues.put(CarsPhotoTable.CARSPHOTO_COLUMN_PATH, photoPath);
		contentValues.put(CarsPhotoTable.CARSPHOTO_COLUMN_CAR_ID, carId);
		getActivity().getContentResolver().insert(CustomersContentProvider.CONTENT_URI_CARSPHOTOS, contentValues);
	}

	private boolean getPermisionStatus()
	{
		return ContextCompat.checkSelfPermission(getActivity(),
				Manifest.permission.CAMERA)
				== PackageManager.PERMISSION_GRANTED;
	}


	private void requestPermissions()
	{
		// Here, thisActivity is the current activity
		if(ContextCompat.checkSelfPermission(getActivity(),
				Manifest.permission.CAMERA)
				!= PackageManager.PERMISSION_GRANTED)
		{
			ActivityCompat.requestPermissions(getActivity(),
					new String[]{Manifest.permission.CAMERA},
					MY_PERMISSIONS_REQUEST_CAMERA);
		}

	}


	@Override
	public void onRequestPermissionsResult(int requestCode,
										   String permissions[], int[] grantResults)
	{
		switch(requestCode)
		{
			case MY_PERMISSIONS_REQUEST_CAMERA:
			{
				// If request is cancelled, the result arrays are empty.
				if(grantResults.length > 0
						&& grantResults[0] == PackageManager.PERMISSION_GRANTED)
				{
					this.addNewPhoto();
				}
				else
				{
					requestPermissions();
				}
				return;
			}
		}
	}


	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{

		super.onActivityResult(requestCode, resultCode, data);

		Log.i(Tag, "Receive the camera result");

		if(resultCode == Activity.RESULT_OK && requestCode == CAMERA_RESULT)
		{
			File out = new File(getActivity().getFilesDir(), MyFileContentProvider.currentImageName);
			if(!out.exists())
			{

				Toast.makeText(getActivity(),
						"Error while capturing image", Toast.LENGTH_LONG)
						.show();
				return;
			}


			photoPaths.add(out.getAbsolutePath());
			photoPager.getAdapter().notifyDataSetChanged();
		}

	}


	@Override
	public void onLoaderReset(Loader<Cursor> loader)
	{

	}


	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated
	 * to the activity and potentially other fragments contained in that
	 * activity.
	 * <p>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface OnFragmentInteractionListener
	{
		// TODO: Update argument type and name
		public void onFragmentInteraction(Uri uri);
	}


	private void setVolumeAdapter()
	{
		String[] brands = getResources().getStringArray(R.array.brandsArray);
		ArrayAdapter<String> brandsAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, brands);
		brandET.setAdapter(brandsAdapter);

		String[] models = getResources().getStringArray(R.array.modelsArray);
		ArrayAdapter<String> modelsAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, models);
		modelET.setAdapter(modelsAdapter);


	}

	@Override
	public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth)
	{
		String date = dayOfMonth+"/"+(monthOfYear+1)+"/"+year;
		dateOfFirstRegistrationET.setText(date);
		dateOfFirstRegistrationET.setFocusableInTouchMode(true);
		dateOfFirstRegistrationET.requestFocus();
	}

	private class CarPhotoPagerAdapter extends PagerAdapter
	{


		@Override
		public int getCount()
		{
			return photoPaths.size();
		}


		@Override
		public boolean isViewFromObject(View view, Object object)
		{
			return view == object;
		}


		public Object getItem(int pos)
		{
			return photoPaths.get(pos);
		}


		@Override
		public Object instantiateItem(ViewGroup container, int position)
		{
			File imgFile = new File((String) getItem(position));
			ImageView image = new ImageView(getActivity());
			Glide.with(image.getContext())
					.load(imgFile) // Uri of the picture
					.into(image);
			container.addView(image);
			return image;
		}


		@Override
		public void destroyItem(ViewGroup container, int position, Object object)
		{
			container.removeView((View) object);
		}


		public long getItemId(int pos)
		{
			return pos;
		}


	}
}



