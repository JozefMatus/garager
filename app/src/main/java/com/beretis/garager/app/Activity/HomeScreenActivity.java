package com.beretis.garager.app.Activity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.NavigationView;
import android.support.v4.view.ViewPager;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.beretis.garager.app.Controller.MySqlLiteHelper;
import com.beretis.garager.app.Fragments.CustomersFragment;
import com.beretis.garager.app.Fragments.PartsFragment;
import com.beretis.garager.app.Fragments.RepairsFragment;
import com.beretis.garager.app.R;
import com.beretis.garager.app.SlidingTabLayout;
import com.beretis.garager.app.Tables.RepairWithPartsViewTable;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;


public class HomeScreenActivity extends AppCompatActivity implements CustomersFragment.OnFragmentInteractionListener, PartsFragment.OnFragmentInteractionListener, RepairsFragment.OnFragmentInteractionListener
{
	//Defining Variables
	private Toolbar toolbar;
	private NavigationView navigationView;
	private DrawerLayout drawerLayout;


	//exporting database
	private void exportDB()
	{
		// TODO Auto-generated method stub

		try
		{
			File sd = Environment.getExternalStorageDirectory();
			File data = Environment.getDataDirectory();

			if(sd.canWrite())
			{
				String currentDBPath = "//data//" + "com.beretis.garager.app"
						+ "//databases//" + MySqlLiteHelper.DATABASE_NAME;
				String backupDBPath = "/BackupFolder/my_garages";
				File currentDB = new File(data, currentDBPath);
				File backupDB = new File(sd, "my_garagesohmyFuck");

				FileChannel src = new FileInputStream(currentDB).getChannel();
				FileChannel dst = new FileOutputStream(backupDB).getChannel();
				dst.transferFrom(src, 0, src.size());
				src.close();
				dst.close();
				Toast.makeText(getBaseContext(), backupDB.toString(),
						Toast.LENGTH_LONG).show();

			}
		}
		catch(Exception e)
		{

			Toast.makeText(getBaseContext(), e.toString(), Toast.LENGTH_LONG)
					.show();

		}
	}


	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home_screen);

		// Initilization
		String test = RepairWithPartsViewTable.DATABASE_CREATE;

		setupActionBar();
		exportDB();

		//Initializing NavigationView
		navigationView = (NavigationView) findViewById(R.id.navigation_view);
		drawerLayout = (DrawerLayout) findViewById(R.id.activity_main_drawer_layout);
		navigationView.getMenu().performIdentifierAction(R.id.drawer_customers, 0);

		setupListeners();
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home_screen, menu);
		return true;
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if(id == R.id.action_settings)
		{
			return true;
		}
		return super.onOptionsItemSelected(item);
	}


	@Override
	public void onFragmentInteraction(String id)
	{

	}


	@Override
	public void onCustomerSelected(int customer)
	{
		startCustomerActivity(customer);
	}




	//Private
	private void setupListeners()
	{
		ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.closeDrawer)
		{

			@Override
			public void onDrawerClosed(View drawerView)
			{
				// Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
				super.onDrawerClosed(drawerView);
			}


			@Override
			public void onDrawerOpened(View drawerView)
			{
				// Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank

				super.onDrawerOpened(drawerView);
			}
		};

		//Setting the actionbarToggle to drawer layout
		drawerLayout.setDrawerListener(actionBarDrawerToggle);

		//Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
		navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener()
		{

			// This method will trigger on item Click of navigation menu
			@Override
			public boolean onNavigationItemSelected(MenuItem menuItem)
			{
				//Checking if the item is in checked state or not, if not make it in checked state
				if(menuItem.isChecked()) menuItem.setChecked(false);
				else menuItem.setChecked(true);

				Fragment fragment = null;
				Class fragmentClass;

				//Check to see which item was being clicked and perform appropriate action
				switch(menuItem.getItemId())
				{
					//Replacing the main content with ContentFragment Which is our Inbox View;
					case R.id.drawer_customers:
						fragmentClass = CustomersFragment.class;
						break;
					case R.id.drawer_repairs:
						fragmentClass = RepairsFragment.class;
						break;
					case R.id.drawer_parts:
						fragmentClass = PartsFragment.class;
						break;
					case R.id.drawer_graphs:
						fragmentClass = CustomersFragment.class;
						break;
					default:
						fragmentClass = RepairsFragment.class;
				}

				try
				{
					fragment = (Fragment) fragmentClass.newInstance();
				}
				catch(InstantiationException e)
				{
					e.printStackTrace();
				}
				catch(IllegalAccessException e)
				{
					e.printStackTrace();
				}


				// Insert the fragment by replacing any existing fragment
				FragmentManager fragmentManager = getFragmentManager();
				fragmentManager.beginTransaction().replace(R.id.containeris, fragment).commit();

				// Highlight the selected item, update the title, and close the drawer
				menuItem.setChecked(true);
				setTitle(menuItem.getTitle());
				//Closing drawer on item click
				drawerLayout.closeDrawers();
				return true;
			}
		});

		actionBarDrawerToggle.syncState();

	}

	private void startCustomerActivity(int customer)
	{
		Intent intent = new Intent(this, CustomerDetailActivity.class);
		intent.putExtra("customerID", customer);
		intent.putExtra("add", false);
		intent.putExtra("edit", false);
		startActivity(intent);
	}


	private void setupActionBar()
	{
		toolbar = (Toolbar) findViewById(R.id.app_bar);
		setSupportActionBar(toolbar);

		ActionBar bar = getSupportActionBar();
		bar.setDisplayUseLogoEnabled(false);
		bar.setDisplayShowTitleEnabled(true);
		bar.setDisplayShowHomeEnabled(true);
		bar.setDisplayHomeAsUpEnabled(false);
		bar.setHomeButtonEnabled(true);
	}

}
