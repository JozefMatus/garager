package com.beretis.garager.app.Controller;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.beretis.garager.app.Tables.CarsDocumentsPhotoTable;
import com.beretis.garager.app.Tables.CarsPhotoTable;
import com.beretis.garager.app.Tables.CarsTable;
import com.beretis.garager.app.Tables.CustomerWithCarsViewTable;
import com.beretis.garager.app.Tables.CustomersTable;
import com.beretis.garager.app.Tables.PartsTable;
import com.beretis.garager.app.Tables.RepairHasPartsTable;
import com.beretis.garager.app.Tables.RepairWithPartsViewTable;
import com.beretis.garager.app.Tables.RepairsTable;


public class MySqlLiteHelper extends SQLiteOpenHelper
{
    public static final String DATABASE_NAME = "garager_ver_9";
    public static final int DATABASE_VERSION = 9;

    public MySqlLiteHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase)
    {
        CustomersTable.onCreate(sqLiteDatabase);
        CarsTable.onCreate(sqLiteDatabase);
        RepairsTable.onCreate(sqLiteDatabase);
        PartsTable.onCreate(sqLiteDatabase);
        RepairHasPartsTable.onCreate(sqLiteDatabase);
        CustomerWithCarsViewTable.onCreate(sqLiteDatabase);
        CarsPhotoTable.onCreate(sqLiteDatabase);
        CarsDocumentsPhotoTable.onCreate(sqLiteDatabase);
        RepairWithPartsViewTable.onCreate(sqLiteDatabase);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion)
    {
        Log.w(MySqlLiteHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
//        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_COMMENTS);
//        onCreate(db);
    }


}
