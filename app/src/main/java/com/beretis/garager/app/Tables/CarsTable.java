package com.beretis.garager.app.Tables;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by beretis on 2.4.14.
 */
public class CarsTable
{
    public static final String CARS_TABLE_NAME = "cars";
    public static final String CARS_COLUMN_BRAND = "brand";
    public static final String CARS_COLUMN_ID = "_id";
    public static final String CARS_COLUMN_MODEL = "model";
    public static final String CARS_COLUMN_VOLUME = "volume";
    public static final String CARS_COLUMN_POWER = "power";
    public static final String CARS_COLUMN_YEAR = "year";
    public static final String CARS_COLUMN_VIN = "VIN";
    public static final String CARS_COLUMN_ENGINE_CODE = "engine_core";
    public static final String CARS_COLUMN_TECH_LICENCE = "tech_licence";
    public static final String CARS_COLUMN_TECH_CHECK = "tech_check";
    public static final String CARS_COLUMN_CUSTOMER_ID = "customer_id";




    static String DATABASE_CREATE = "CREATE TABLE " + CARS_TABLE_NAME + " (" +
            CARS_COLUMN_ID + " INTEGER primary key autoincrement," +
            CARS_COLUMN_BRAND + " TEXT NOT NULL," +
            CARS_COLUMN_MODEL + " TEXT NOT NULL," +
            CARS_COLUMN_YEAR + " INTEGER NOT NULL," +
            CARS_COLUMN_VOLUME + " INTEGER NOT NULL," +
            CARS_COLUMN_POWER + " INTEGER NOT NULL," +
            CARS_COLUMN_VIN + " TEXT," +
            CARS_COLUMN_ENGINE_CODE + " TEXT," +
            CARS_COLUMN_TECH_CHECK + " INTEGER," +
            CARS_COLUMN_TECH_LICENCE + " TEXT," +
            CARS_COLUMN_CUSTOMER_ID + " INTEGER NOT NULL," +
            "FOREIGN KEY (" + CARS_COLUMN_CUSTOMER_ID + ") REFERENCES " + CustomersTable.CUSTOMERS_TABLE_NAME + "(" + CustomersTable.CUSTOMERS_COLUMN_ID + "));";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.w(CarsTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + CARS_TABLE_NAME);
        onCreate(database);
    }
}
