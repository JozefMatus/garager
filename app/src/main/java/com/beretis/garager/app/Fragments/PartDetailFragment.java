package com.beretis.garager.app.Fragments;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.beretis.garager.app.Activity.ScannerActivity;
import com.beretis.garager.app.CustomersContentProvider;
import com.beretis.garager.app.Fragments.Helper.Enums;
import com.beretis.garager.app.R;
import com.beretis.garager.app.Tables.PartsTable;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PartDetailFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PartDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PartDetailFragment extends Fragment
{
    public static final int SCANNER_REQUEST_CODE = 1000;

    private static final String ARG_CREATION_TYPE = "creationType";
    private static final String ARG_ID = "id";

    private EditText warrantyET, codeET, titleET, supplierET, typeET;
    private Button scanButton;
    private Enums.FragmentCreationType mCreationType;
    private int id;
    private boolean setup = true;

    private OnFragmentInteractionListener mListener;

    public static PartDetailFragment newInstance(Enums.FragmentCreationType creationType, int id)
    {
        PartDetailFragment fragment = new PartDetailFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_CREATION_TYPE, creationType.value);
        args.putInt(ARG_ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    public PartDetailFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
        {
            id = getArguments().getInt(ARG_ID);
            mCreationType = Enums.FragmentCreationType.fromInt(getArguments().getInt(ARG_CREATION_TYPE));
        }
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_part_detail, container, false);
        findElementsFromView(view);
        return view;
    }


    private void findElementsFromView(View view)
    {
        scanButton = (Button) view.findViewById(R.id.fg_part_detail_bt_scan);
        codeET = (EditText) view.findViewById(R.id.fg_part_detail_et_id);
        warrantyET = (EditText) view.findViewById(R.id.fg_part_detail_et_warranty);
        typeET = (EditText) view.findViewById(R.id.fg_part_detail_et_parttype);
        titleET = (EditText) view.findViewById(R.id.fg_part_detail_et_partname);

        supplierET = (EditText) view.findViewById(R.id.fg_part_detail_et_supplier);
        scanButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(getActivity(), ScannerActivity.class);
                startActivityForResult(intent, SCANNER_REQUEST_CODE);
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri)
    {
        if (mListener != null)
        {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try
        {
            mListener = (OnFragmentInteractionListener) activity;
        }
        catch (ClassCastException e)
        {
//            throw new ClassCastException(activity.toString()
//                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener
    {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (resultCode != Activity.RESULT_OK)
        {
            return;
        }
        if (requestCode == SCANNER_REQUEST_CODE)
        {
            this.codeET.setText(data.getExtras().getString("barecode"));
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        if (mCreationType == Enums.FragmentCreationType.ADD)
        {
            MenuItem mi = menu.add(Menu.NONE, 586, Menu.NONE, "picusko");
            mi.setIcon(android.R.drawable.ic_menu_add);
            mi.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        }
        else
        {
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())//todo add more actions
        {
            case 586:
            {
                savePartToDb();
                return true;
            }
            default:
            {
                return super.onOptionsItemSelected(item);
            }
        }

    }

    private void savePartToDb()
    {

//        static String DATABASE_CREATE = "CREATE TABLE " + PARTS_TABLE_NAME + " (" +
//                PARTS_COLUMN_ID + " TEXT primary key NOT NULL," +
//                PARTS_COLUMN_TITLE + " TEXT NOT NULL," +
//                PARTS_COLUMN_PURCHASE + " TEXT NOT NULL," +
//                PARTS_COLUMN_CATEGORY + " TEXT NOT NULL," +
//                PARTS_COLUMN_SELL + " TEXT NOT NULL," +
//                PARTS_COLUMN_BARECODE + " TEXT," +
//                PARTS_COLUMN_SUPPLIER + " TEXT NOT NULL);";

        ContentValues contentValues = new ContentValues();
        contentValues.put(PartsTable.PARTS_COLUMN_TITLE, String.valueOf(titleET.getText()));
        contentValues.put(PartsTable.PARTS_COLUMN_CATEGORY, String.valueOf(typeET.getText()));
        contentValues.put(PartsTable.PARTS_COLUMN_BARECODE, String.valueOf(codeET.getText()));
        contentValues.put(PartsTable.PARTS_COLUMN_ID, String.valueOf(codeET.getText()));
        contentValues.put(PartsTable.PARTS_COLUMN_SUPPLIER, String.valueOf(supplierET.getText()));
        contentValues.put(PartsTable.PARTS_COLUMN_SELL, String.valueOf(supplierET.getText()));
        contentValues.put(PartsTable.PARTS_COLUMN_PURCHASE, String.valueOf(supplierET.getText()));
        getActivity().getContentResolver().insert(CustomersContentProvider.CONTENT_URI_PARTS, contentValues);
        getActivity().finish();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu)
    {

//        if (mCreationType == Enums.FragmentCreationType.ADD && setup)
//        {
//            MenuItem mi = menu.add("New Item");
//            mi.setIcon(android.R.drawable.ic_menu_add);
//            mi.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_IF_ROOM);
//            mi.set
//            setup = !setup;
//        }
//        else
//        {
//        }


        super.onPrepareOptionsMenu(menu);
    }
}
