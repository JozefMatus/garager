package com.beretis.garager.app.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.beretis.garager.app.R;
import com.beretis.garager.app.Tables.CarsTable;

/**
 * Created by jozefmatus on 1/9/15.
 */
public class CarsCursorAdapter extends CursorAdapter
{
    LayoutInflater layoutInflater;

    public CarsCursorAdapter(Context context, Cursor c, boolean autoRequery)
    {
        super(context, c, autoRequery);
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public CarsCursorAdapter(Context context, Cursor c, int flags)
    {
        super(context, c, flags);
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent)
    {
        return layoutInflater.inflate(R.layout.li_car, null);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor)
    {
        ImageView thumbnail = (ImageView) view.findViewById(R.id.li_car_iv_thumbnail);
        TextView brand = (TextView) view.findViewById(R.id.li_car_tv_brand);
        TextView model = (TextView) view.findViewById(R.id.li_car_tv_model);
        TextView year = (TextView) view.findViewById(R.id.li_car_tv_year);

        brand.setText(cursor.getString(cursor.getColumnIndex(CarsTable.CARS_COLUMN_BRAND)));
        model.setText(cursor.getString(cursor.getColumnIndex(CarsTable.CARS_COLUMN_MODEL)));
//        year.setText(String.valueOf(cursor.getInt(cursor.getColumnIndex(CarsTable.CARS_COLUMN_YEAR))));
//        year.setText(String.valueOf(cursor.getInt(1999)));


    }
}
