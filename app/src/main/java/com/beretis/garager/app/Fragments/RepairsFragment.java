package com.beretis.garager.app.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.beretis.garager.app.Activity.AddRepairActivity;
import com.beretis.garager.app.Activity.PartDetailActivity;
import com.beretis.garager.app.Activity.RepairDetailActivity;
import com.beretis.garager.app.Adapters.CustomersCursorAdapter;
import com.beretis.garager.app.Adapters.RepairsCursorAdapter;
import com.beretis.garager.app.CustomersContentProvider;
import com.beretis.garager.app.R;
import com.beretis.garager.app.Tables.CustomersTable;
import com.beretis.garager.app.Tables.RepairWithPartsViewTable;
import com.beretis.garager.app.Tables.RepairsTable;

import java.util.Date;


public class RepairsFragment extends Fragment implements AbsListView.OnItemClickListener, LoaderManager.LoaderCallbacks<Cursor>
{

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private RepairsCursorAdapter mAdapter;

    private OnFragmentInteractionListener mListener;

    /**
     * The fragment's ListView/GridView.
     */
    private AbsListView mListView;

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */

    // TODO: Rename and change types of parameters
    public static RepairsFragment newInstance(String param1, String param2)
    {
        RepairsFragment fragment = new RepairsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public RepairsFragment()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {

//        REPAIRS_COLUMN_ID + " INTEGER primary key autoincrement," +
//                REPAIRS_COLUMN_SUBJECT + " TEXT NOT NULL," +
//                REPAIRS_COLUMN_INFO + " TEXT," +
//                REPAIRS_COLUMN_ISMAINTENANCE + " BOOLEAN NOT NULL CHECK (" + REPAIRS_COLUMN_ISMAINTENANCE + " IN (0,1))," +
//                REPAIRS_COLUMN_PRICE + " INTEGER NOT NULL," +
//                REPAIRS_COLUMN_TIME_PERIOD + " INTEGER," +
//                REPAIRS_COLUMN_DISTANCE_PERIOD + " INTEGER," +
//                REPAIRS_COLUMN_DATE + " INTEGER NOT NULL," +
//                REPAIRS_COLUMN_CAR + " INTEGER NOT NULL," +
//                REPAIRS_COLUMN_CUSTOMER + " INTEGER NOT NULL," +



        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null)
        {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_repairsfragment, container, false);

        // Set the adapter
        mListView = (AbsListView) view.findViewById(android.R.id.list);

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);
        fillData();
        return view;
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        try
        {
            mListener = (OnFragmentInteractionListener) activity;
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.menu_repairs, menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        switch (item.getItemId())
        {
            case R.id.action_add_repair:

                Intent intent = AddRepairActivity.newIntent(getActivity());
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
    @Override
    public void onDetach()
    {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {

        int selectedId = mAdapter.getCursor().getInt(mAdapter.getCursor().getColumnIndex(RepairsTable.REPAIRS_COLUMN_ID));
        Intent intent = new RepairDetailActivity().newIntent(getActivity(), selectedId, true);
        startActivity(intent);

    }

    /**
     * The default content for this Fragment has a TextView that is shown when
     * the list is empty. If you would like to change the text, call this method
     * to supply the text it should use.
     */
    public void setEmptyText(CharSequence emptyText)
    {
        View emptyView = mListView.getEmptyView();

        if (emptyText instanceof TextView)
        {
            ((TextView) emptyView).setText(emptyText);
        }
    }

    private void fillData()
    {
//        cursor = context.getContentResolver().query(MyContentProvider.CONTENT_URI, null, null, null, MyContentProvider.NAME);
//        mAdapter = new CustomersCursorAdapter(context, cursor, true);

        getLoaderManager().initLoader(0, null, this);
        mAdapter = new RepairsCursorAdapter(getActivity(), null, 0);
        mListView.setAdapter(mAdapter);

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args)
    {
        CursorLoader cursorLoader = new CursorLoader(getActivity(),
                CustomersContentProvider.CONTENT_URI_REPAIRS, null, null, null, CustomersContentProvider.PROVIDER_NAME);
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data)
    {
        mAdapter.swapCursor(data);

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader)
    {
        mAdapter.swapCursor(null);

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener
    {
        // TODO: Update argument type and name
        public void onFragmentInteraction(String id);
    }

}
