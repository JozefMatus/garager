package com.beretis.garager.app.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.beretis.garager.app.R;
import com.beretis.garager.app.Services.ColorPicker;
import com.beretis.garager.app.Tables.CustomersTable;

import org.w3c.dom.Text;

/**
 * Created by beretis on 31.3.14.
 */
public class CustomersCursorAdapter extends CursorAdapter
{
    LayoutInflater mInflater;
    // declare the builder object once.
    TextDrawable.IBuilder builder = TextDrawable.builder()
            .round();
    public CustomersCursorAdapter(Context context, Cursor c, boolean autoRequery)
    {
        super(context, c, autoRequery);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public CustomersCursorAdapter(Context context, Cursor c, int flags)
    {
        super(context, c, flags);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup)
    {

        View view = mInflater.inflate(R.layout.li_customer,null);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor)
    {
        TextView name = (TextView) view.findViewById(R.id.TW_customer_name);
        TextView contact = (TextView) view.findViewById(R.id.TW_customer_contact);
        TextView numberOfCars = (TextView) view.findViewById(R.id.TW_customer_number_of_cars);
        ImageView contactIco = (ImageView) view.findViewById(R.id.IW_customer_photo);

        String text = new String(cursor.getString(cursor.getColumnIndex(CustomersTable.CUSTOMERS_COLUMN_NAME)));
        String firstLetter = text.substring(0,1).toUpperCase();
        name.setText(text);
        int wut = text.hashCode();
        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
        int color = ColorPicker.getColorFromInt(wut);
        TextDrawable icon = builder.build(firstLetter, color);
        contactIco.setImageDrawable(icon);
    }



}
